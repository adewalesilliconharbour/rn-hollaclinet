import React, {Component} from 'react';
import {View, KeyboardAvoidingView} from 'react-native';
import {Item, Input, Text} from 'native-base';
import DatePicker from 'react-native-datepicker';
import {connect} from 'react-redux';
import {sendMessage} from '../store/actions/index';
import {Icon, Button} from 'react-native-elements';
import io from 'socket.io-client';
import api from '../assets/connection';

export class InputMessage extends Component {
  typingmessage;
  state = {
    message: '',
    id: '',
  };

  componentDidMount() {
    this.socket = io(`${api.url}`);
  }

  typing = () => {
    const {clients} = this.props;
    this.socket.emit('start_typing', {
      receiver: clients.worker.id,
    });
    if (this.typingmessage) {
      clearTimeout(this.typingmessage);
    }
    this.typingmessage = setTimeout(() => {
      this.socket.emit('stop_typing', {
        receiver: clients.worker.id,
      });
    }, 500);
  };

  sendMessage = () => {
    const {clients, send} = this.props;

    var data = {
      id: clients.id,
      receciver: clients.worker.id,
      message: this.state.message,
    };
    if (this.state.message != '') {
      send(data);
      this.setState({message: ''});
    } else {
      null;
    }
  };

  render() {
    return (
      <KeyboardAvoidingView behavior="position" enabled>
        <Button
          title="End Chat"
          onPress={() => this.props.navigation.navigate('confirm')}
        />
        <View
          style={{
            flexDirection: 'row',
            paddingHorizontal: 10,
            backgroundColor: '#fff',
            shadowOpacity: 0.2,
            shadowRadius: 30,
            paddingVertical: 10,
          }}>
          <Item
            style={{
              flex: 1,
              borderRadius: 30,
              marginTop: 6,
              height: 0,
              paddingVertical: 20,
              paddingHorizontal: 10,
            }}
            regular>
            <Input
              value={this.state.message}
              onChange={() => this.typing()}
              onChangeText={message => this.setState({message})}
              placeholder="Type a message...."
            />
          </Item>
          <Icon
            size={20}
            raised
            reverse
            name="send"
            onPress={() => this.sendMessage()}
          />
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const mapStateToProps = state => {
  return {
    clients: state.job.workerState,
    chats: state.job.workerState.conversation,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    send: payload => dispatch(sendMessage(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(InputMessage);
