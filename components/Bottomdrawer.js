import React, {Component} from 'react';
import {Text, View, Image, Dimensions} from 'react-native';
import SlidingUpPanel from 'rn-sliding-up-panel';
import {Divider, Input} from 'react-native-elements';
const {height} = Dimensions.get('window');
import location from '../assets/location.png';

export class Bottomdrawer extends Component {
  bookingUI(state) {
    switch (state) {
      case 'request':
        return (
          <SlidingUpPanel
            ref={c => (this._panel = c)}
            backdropOpacity={0.4}
            animatedValue={this._draggedValue}
            friction={3}
            allowMomentum
            showBackdrop
            containerStyle={{
              shadowRadius: 12,
              shadowOffset: {width: 1, height: 1},
              shadowOpacity: 0.3,
            }}
            draggableRange={{top: height / 1.2, bottom: 300}}>
            <View
              style={{
                backgroundColor: 'white',
                height: '100%',
                borderTopRightRadius: 24,
                borderTopLeftRadius: 24,
              }}>
              <Divider
                style={{
                  height: 6,
                  borderRadius: 20,
                  marginTop: 10,
                  marginHorizontal: '42%',
                }}
              />
              <View style={{paddingHorizontal: 10}}>
                <View style={{flexDirection: 'row', marginTop: 40}}>
                  <View style={{width: 42}}>
                    <Image
                      source={location}
                      style={{
                        width: 30,
                        bottom: 110,
                        left: 10,
                        resizeMode: 'contain',
                      }}
                    />
                  </View>
                  <View style={{flex: 1}}>
                    <Input
                      containerStyle={{marginBottom: 15}}
                      label="MY LOCATION"
                      labelStyle={{fontSize: 14, fontWeight: '500'}}
                      inputStyle={{fontSize: 13}}
                      placeholder="My Location"
                      value="My Current Location"
                    />
                    <Input
                      label="SEARCH FOR WORKER"
                      labelStyle={{fontSize: 14, fontWeight: '500'}}
                      inputStyle={{fontSize: 13}}
                      placeholder="e.g plumber, carpenter, bricklayer, etc"
                      clearButtonMode="always"
                    />
                  </View>
                </View>
              </View>
            </View>
          </SlidingUpPanel>
        );
      default:
        return null;
    }
  }

  componentDidMount() {
    console.log(this.props);
  }

  render() {
    const {state} = this.props;
    return <View>{this.bookingUI(state)}</View>;
  }
}

export default Bottomdrawer;
