import React, {Component} from 'react';
import {Dimensions, View, Image} from 'react-native';
import Pin from '../assets/pin.png';
import MapStyle from '../assets/map_style.json';
import MapView, {
  PROVIDER_GOOGLE,
  Marker,
  AnimatedRegion,
} from 'react-native-maps';
import Geolocation from 'react-native-geolocation-service';
import {connect} from 'react-redux';
import io from 'socket.io-client';
import api from '../assets/connection';
import {workerLocation} from '../store/actions/index';
import {Container} from 'native-base';

const {width, height} = Dimensions.get('window');

const SCREEN_HEIGHT = height;
const SCREEN_WIDTH = width;
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

export class Maps extends Component {
  constructor(props) {
    super(props);
    this.state = {
      initialRegion: new AnimatedRegion({
        latitude: 37.78825,
        longitude: -122.4324,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
      }),
    };
  }

  componentDidMount() {
    this.socket = io(`${api.url}`);
    const {locations} = this.props;
    locations();
    this._myLocation();
    this.socket.on('new_location', () => {
      locations();
    });
  }

  _myLocation = async () => {
    await Geolocation.watchPosition(
      res => {
        var lat = parseFloat(res.coords.latitude);
        var long = parseFloat(res.coords.longitude);
        const newregion = {
          latitude: lat,
          longitude: long,
          latitudeDelta: LATITUDE_DELTA,
          longitudeDelta: LONGITUDE_DELTA,
        };
        this.map.animateToRegion(newregion, 500);
        this.setState({
          initialRegion: newregion,
        });
      },
      err => console.log(err),
      {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000},
    );
  };

  render() {
    const {worker} = this.props;
    return (
      <View>
        <MapView
          ref={map => {
            this.map = map;
          }}
          customMapStyle={MapStyle}
          provider={PROVIDER_GOOGLE}
          showsUserLocation
          collapsable
          followsUserLocation
          zoomEnabled
          pitchEnabled
          toolbarEnabled
          minZoomLevel={15}
          loadingEnabled
          showsMyLocationButton
          mapPadding={{bottom: 310}}
          style={{width: '100%', height: '100%'}}
          initialRegion={this.state.initialRegion}>
          {worker.length == 0
            ? null
            : worker.map((marker, i) => (
                <MapView.Marker.Animated
                  key={i}
                  coordinate={{
                    latitude: marker.my_location.coordinates[1],
                    longitude: marker.my_location.coordinates[0],
                  }}>
                  <Image
                    source={Pin}
                    style={{height: 30, resizeMode: 'contain'}}
                  />
                </MapView.Marker.Animated>
              ))}
        </MapView>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    worker: state.location.user,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    locations: () => dispatch(workerLocation()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Maps);
