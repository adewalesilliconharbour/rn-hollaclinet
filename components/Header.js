import React, {Component} from 'react';
import {Text, View, StatusBar, Dimensions} from 'react-native';
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Button,
  Title,
  Icon,
  DatePicker,
  Fab,
} from 'native-base';
import {Avatar, Badge} from 'react-native-elements';

const screenWidth = Math.round(Dimensions.get('window').width);

export class HeaderNav extends Component {
  leftSide(title) {
    switch (title) {
      case 'Notification':
        return (
          <Button rounded icon style={{backgroundColor: '#3286C4'}}>
            <Icon name="trash" fontSize={80} />
          </Button>
        );
      case 'History':
        return (
          <Button rounded style={{backgroundColor: '#3286C4'}}>
            <DatePicker
              minimumDate={new Date(2020, 1, 1)}
              locale="en"
              modalTransparent={true}
              textStyle={{color: '#fff', bottom: 4}}
              placeHolderTextStyle={{color: '#fff', bottom: 4}}
              animationType="fade"
              formatChosenDate={date => date.toString().substr(4, 12)}
            />
            <Icon name="arrow-down" />
          </Button>
        );
      case 'My Wallet':
        return (
          <Button
            rounded
            style={{backgroundColor: '#3286C4', paddingHorizontal: 20}}>
            <Badge status="warning" value=" " />
            <Text style={{color: '#fff', fontSize: 19, marginLeft: 15}}>
              {' '}
              <Text style={{fontWeight: '700'}}>{'\u20A6'}</Text> 6500
            </Text>
          </Button>
        );
      case 'My Account':
        return <Avatar rounded icon={{name: 'home'}} size="medium" />;
      case 'Edit Profile':
        return (
          <Button rounded icon style={{backgroundColor: '#3286C4'}}>
            <Icon name="save" fontSize={80} />
          </Button>
        );
      default:
        return null;
    }
  }

  height(title) {
    switch (title) {
      case 'My Wallet':
        return 300;
      case 'History':
        return 230;
      default:
        return 160;
    }
  }

  backbutton(title, navigation) {
    switch (title) {
      case 'My Account':
        return null;
      case 'Edit Profile':
        return null;
      default:
        return (
          <Button light transparent onPress={() => navigation.openDrawer()}>
            <Icon name="arrow-back" />
          </Button>
        );
    }
  }

  render() {
    const {title, navigation} = this.props;

    var headiing;

    if (title == 'Invite Friends') {
      headiing = (
        <View>
          <Header transparent>
            <Left>{this.backbutton(title, navigation)}</Left>
            <Body>
              <Title style={{color: '#fff'}}>{title}</Title>
            </Body>
            <Right />
          </Header>
        </View>
      );
    } else {
      headiing = (
        <View style={{height: this.height(title), backgroundColor: '#358DCE'}}>
          <Header transparent>
            <Left>{this.backbutton(title, navigation)}</Left>
          </Header>
          <Body
            style={{
              top: 90,
              alignItems: 'flex-start',
              width: '100%',
              paddingHorizontal: 10,
              position: 'absolute',
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <Title
              style={{
                fontSize: 34,
                fontWeight: '500',
                textAlign: 'right',
                color: '#fff',
              }}>
              {title}
            </Title>
            {this.leftSide(title)}
          </Body>
        </View>
      );
    }

    return <View>{headiing}</View>;
  }
}

export default HeaderNav;
