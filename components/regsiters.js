import React, {Component} from 'react';
import {Text, View} from 'react-native';
import {Form} from 'native-base';
import styles from '../assets/styles';
import {Input, Button} from 'react-native-elements';
import {connect} from 'react-redux';
import {authSignIn} from '../store/actions/index';
import PhoneInput from 'react-native-phone-input';

export class Regsiters extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      phone: '',
      country_code: '+234',
      rights: 'client',
      hasError: false,
    };
    this.onSubmit.bind();
  }

  componentDidUpdate() {
    const {response, navigation} = this.props;
    if (response.isSuccess) {
      navigation.navigate('VerifyOtp');
    }
  }

  phoneError(phone, country) {
    if (phone == '') {
      return <Text style={styles.errorInput}>Phone Number is required</Text>;
    } else if (phone.length != 11 || isNaN(phone)) {
      return (
        <Text style={styles.errorInput}>
          Please enter a correct Phone Number
        </Text>
      );
    } else if (country != '+234') {
      return (
        <Text style={styles.errorInput}>
          These country is not open to these service.
        </Text>
      );
    } else {
      return null;
    }
  }

  onSubmit() {
    const {onSignIn} = this.props;
    var logiInData = {
      phone_no: this.state.phone,
      country_code: this.state.country_code,
      rights: this.state.rights,
    };
    if (this.phoneError(logiInData.phone_no, logiInData.country_code) != null) {
      this.setState({hasError: true});
    } else {
      onSignIn(logiInData);
    }
  }
  render() {
    return (
      <View>
        <Form style={styles.form}>
          <View style={{marginBottom: 20}}>
            <PhoneInput
              ref="phone"
              style={styles.phoneInput}
              initialCountry="ng"
              cancelText="Dismiss"
              confirmText="Choose"
              textProps={{placeholder: 'Mobile Number'}}
              offset={20}
              onChangePhoneNumber={phone =>
                this.setState({phone, hasError: false})
              }
              onSelectCountry={country_code =>
                country_code == 'ng'
                  ? this.setState({
                      country_code: '+234',
                      hasError: false,
                    })
                  : this.setState({country_code, hasError: false})
              }
              textStyle={{fontSize: 18}}
              allowZeroAfterCountryCode={true}
            />
            {this.state.hasError
              ? this.phoneError(this.state.phone, this.state.country_code)
              : null}
          </View>
          <View>
            <Button
              title="NEXT"
              buttonStyle={styles.buttonStyle}
              onPress={() => this.onSubmit()}
            />
          </View>
        </Form>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    response: state.response,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onSignIn: payload => dispatch(authSignIn(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Regsiters);
