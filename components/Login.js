import React, {Component} from 'react';
import {View, Text} from 'react-native';
import {Input, Button} from 'react-native-elements';
import styles from '../assets/styles';
import PhoneInput from 'react-native-phone-input';
import {Form} from 'native-base';
import {connect} from 'react-redux';
import {authSignUp} from '../store/actions/index';
import api from '../assets/connection';

export class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      phone: '',
      country_code: '+234',
      rights: 'client',
      hasError: false,
    };
    this.onSubmit.bind();
  }

  componentDidUpdate() {
    const {response, navigation} = this.props;
    if (response.isSuccess) {
      navigation.navigate('VerifyOtp');
    }
  }

  phoneError(phone, country) {
    if (phone == '') {
      return <Text style={styles.errorInput}>Phone Number is required</Text>;
    } else if (phone.length != 11 || isNaN(phone)) {
      return (
        <Text style={styles.errorInput}>
          Please enter a correct Phone Number
        </Text>
      );
    } else if (country != '+234') {
      return (
        <Text style={styles.errorInput}>
          These country is not open to these service.
        </Text>
      );
    } else {
      return null;
    }
  }

  emailError(email) {
    rejex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (email == '') {
      return <Text style={styles.errorInput}>Email is required</Text>;
    } else if (!rejex.test(email)) {
      return (
        <Text style={styles.errorInput}>
          Please enter a correct Email address
        </Text>
      );
    } else {
      return null;
    }
  }

  onSubmit() {
    const {onRegister} = this.props;
    var logiInData = {
      email: this.state.email,
      phone_no: this.state.phone,
      country_code: this.state.country_code,
      rights: this.state.rights,
    };
    if (
      this.emailError(logiInData.email) != null ||
      this.phoneError(logiInData.phone_no, logiInData.country_code) != null
    ) {
      this.setState({hasError: true});
    } else {
      onRegister(logiInData);
    }
  }
  render() {
    return (
      <View>
        <Form style={styles.form}>
          <View style={{marginBottom: 20}}>
            <Input
              placeholder="name@example.com"
              autoCapitalize="none"
              onChangeText={email => this.setState({email, hasError: false})}
              value={this.state.email}
              containerStyle={styles.formConstainerStyle}
              inputStyle={styles.inputStyle}
              inputContainerStyle={styles.inputContainerStyle}
            />
            {this.state.hasError ? this.emailError(this.state.email) : null}
          </View>
          <View style={{marginBottom: '4%'}}>
            <PhoneInput
              ref="phone"
              style={styles.phoneInput}
              initialCountry="ng"
              cancelText="Dismiss"
              confirmText="Choose"
              textProps={{placeholder: 'Mobile Number'}}
              offset={20}
              onChangePhoneNumber={phone =>
                this.setState({phone, hasError: false})
              }
              onSelectCountry={country_code =>
                country_code == 'ng'
                  ? this.setState({country_code: '+234', hasError: false})
                  : this.setState({country_code, hasError: false})
              }
              textStyle={{fontSize: 18}}
              allowZeroAfterCountryCode={true}
            />
            {this.state.hasError
              ? this.phoneError(this.state.phone, this.state.country_code)
              : null}
          </View>
          <View>
            <Button
              title="Sign Up"
              buttonStyle={styles.buttonStyle}
              onPress={() => this.onSubmit()}
            />
          </View>
        </Form>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    response: state.response,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onRegister: userdata => dispatch(authSignUp(userdata)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
