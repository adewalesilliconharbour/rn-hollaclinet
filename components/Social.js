import React, {Component} from 'react';
import {Text, View} from 'react-native';
import {SocialIcon, Button} from 'react-native-elements';
import styles from '../assets/styles';

export class Social extends Component {
  render() {
    return (
      <View>
        <SocialIcon
          style={{borderRadius: 16}}
          title="Connect with Facebook"
          button
          onPress={() =>alert('Service currently not available!!!')}
          type="facebook"
        />
        <Button
          titleStyle={{fontSize: 14, color: '#000'}}
          onPress={() => {}}
          title="By clicking start, you agree to our Terms and Conditions"
          type="clear"
        />
      </View>
    );
  }
}

export default Social;
