import React, {Component, useEffect} from 'react';
import {Text, View, ScrollView, ImageBackground} from 'react-native';
import {Avatar, Button, Icon, ListItem} from 'react-native-elements';
import sidebg from '../assets/sidebg.png';
import {connect} from 'react-redux';
import {DrawerItems} from 'react-navigation-drawer';

const DrawerComp = props => (
  // useEffect(() => {
  //   console.log(props);
  // }),
  <View>
    <View>
      <ImageBackground
        source={sidebg}
        style={{
          width: '100%',
          height: 280,
          resizeMode: 'contain',
        }}>
        <View style={{marginHorizontal: 30, marginTop: 70}}>
          <Avatar rounded size={100} icon={{name: 'home'}} />
          {username(props)}
          <ListItem
            containerStyle={{
              width: 150,
              paddingVertical: 6,
              paddingHorizontal: 22,
              borderRadius: 20,
            }}
            title={
              <Text style={{color: '#191919', fontSize: 16}}>
                Cash <Text style={{fontWeight: '700'}}>0 </Text>
              </Text>
            }
            chevron
          />
        </View>
      </ImageBackground>
    </View>
    <ScrollView style={{paddingTop: 40}}>
      <DrawerItems {...props} activeTintColor="#BEC2CE" />
    </ScrollView>
  </View>
);

const mapStateToProps = state => {
  return {
    user: state.user.user,
  };
};

const username = props => {
  if (props.user) {
    return (
      <Text
        numberOfLines={1}
        style={{marginVertical: 15, color: '#fff', fontSize: 20}}>
        {`${props.user.firstName} ${props.user.lastName}`}
      </Text>
    );
  } else {
    return <Text>Loading</Text>;
  }
};

export default connect(mapStateToProps)(DrawerComp);
