import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {createDrawerNavigator} from 'react-navigation-drawer';
import {Container, Header, Left, Button} from 'native-base';
import {connect} from 'react-redux';
import {
  createReduxContainer,
  createReactNavigationReduxMiddleware,
  createNavigationReducer,
} from 'react-navigation-redux-helpers';
import {Icon} from 'react-native-elements';
import React, {Component} from 'react';
import Auth from '../screens/Auth';
import Otp from '../screens/Otp';
import Home from '../screens/Home';
import AuthLoadingScreen from '../screens/AuthLoadingPage';
import Wallet from '../screens/Wallet';
import History from '../screens/History';
import Notification from '../screens/Notification';
import Invite from '../screens/Invite';
import Settings from '../screens/Settings';
import Logout from '../screens/Logout';
import DrawerComp from './drawer';
import Profile from '../screens/Profile';
import Editprofile from '../screens/Editprofile';
import terms from '../assets/terms';
import Terms from '../screens/Terms';
import {Chat} from '../screens/Chat';
import PriceList from '../screens/PriceList';
import Review from '../screens/Review';
import ChatPage from '../screens/ChatPage';
import Epayment from '../screens/E-payment';
import ConfirmPage from '../screens/ConfirmPage';

const AppNavigator = createStackNavigator(
  {
    Login: {
      screen: Auth,
      navigationOptions: {
        title: '',
      },
    },
    VerifyOtp: {
      screen: Otp,
      navigationOptions: {
        headerBackTitleVisible: false,
        headerTransparent: true,
        headerTintColor: '#fff',
      },
    },
  },
  {
    initialRouteName: 'Login',
    defaultNavigationOptions: {
      title: '',
      headerTransparent: true,
      headerTitleAlign: 'center',
      headerTintColor: '#fff',
    },
  },
);

const HomeStack = createStackNavigator(
  {
    Home: {
      screen: Home,
      //   navigationOptions: ({navigation}) => ({
      //     headerShown: true,
      //     headerTransparent: true,
      //   }),
    },
    chat: {
      screen: ChatPage,
      headerTransparent: true,
      navigationOptions: ({navigation}) => ({
        title: '',
        headerTintColor: '#fff',
      }),
    },
    price: {
      screen: PriceList,
      navigationOptions: {
        title: 'Price Details',
        headerShown: true,
      },
    },
    review: {
      screen: Review,
      navigationOptions: {
        title: 'Review',
        headerShown: true,
      },
    },
    payment: {
      screen: Epayment,
    },
    confirm: {
      screen: ConfirmPage,
    },
  },
  {
    initialRouteName: 'Home',
    defaultNavigationOptions: {
      header: () => null,
      headerTransparent: true,
      headerShown: false,
    },
  },
);

const SettingsStack = createStackNavigator(
  {
    settings: {
      screen: Settings,
    },
    profile: {
      screen: Profile,
    },
    editProfile: {
      screen: Editprofile,
    },
    terms: {
      screen: Terms,
      navigationOptions: {
        headerTintColor: '#000',
      },
    },
  },
  {
    initialRouteName: 'settings',
    defaultNavigationOptions: {
      headerTransparent: true,
      title: '',
      headerBackTitleVisible: false,
      headerTintColor: '#fff',
    },
  },
);

const AppStack = createDrawerNavigator(
  {
    Home: {
      screen: HomeStack,
      navigationOptions: {
        drawerIcon: () => <Icon name="home" style={{color: '#BEC2CE'}} />,
        title: 'Home',
        drawerLabel: 'Home',
      },
    },
    Wallet: {
      screen: Wallet,
      navigationOptions: {
        drawerIcon: () => <Icon name="home" style={{color: '#BEC2CE'}} />,
        title: '',
        drawerLabel: 'My Wallet',
      },
    },
    History: {
      screen: History,
      navigationOptions: {
        drawerIcon: () => <Icon name="home" style={{color: '#BEC2CE'}} />,
        title: '',
        drawerLabel: 'History',
      },
    },
    Notfication: {
      screen: Notification,
      navigationOptions: {
        drawerIcon: () => <Icon name="home" style={{color: '#BEC2CE'}} />,
        title: '',
        drawerLabel: 'Notification',
      },
    },
    Invite: {
      screen: Invite,
      navigationOptions: {
        drawerIcon: () => <Icon name="home" style={{color: '#BEC2CE'}} />,
        title: '',
        drawerLabel: 'Invite Friends',
      },
    },
    Settings: {
      screen: SettingsStack,
      navigationOptions: {
        drawerIcon: () => <Icon name="home" style={{color: '#BEC2CE'}} />,
        title: '',
        drawerLabel: 'Settings',
      },
    },
    LogOut: {
      screen: Logout,
      navigationOptions: {
        drawerIcon: () => <Icon name="home" style={{color: '#BEC2CE'}} />,
        title: '',
        drawerLabel: 'Log Out',
      },
    },
  },
  {
    initialRouteName: 'Home',
    drawerType: 'front',
    contentComponent: DrawerComp,
  },
);

const AuthStack = createSwitchNavigator(
  {
    AuthLoading: {
      screen: AuthLoadingScreen,
    },
    App: {
      screen: AppStack,
    },
    Auth: {
      screen: AppNavigator,
    },
  },
  {
    initialRouteName: 'AuthLoading',
    initialRouteParams: 'AuthLoading',
    defaultNavigationOptions: {
      headerTransparent: true,
    },
  },
);

const AppContainer = createAppContainer(AuthStack);

export const navReducer = createNavigationReducer(AppContainer);

export const middleware = createReactNavigationReduxMiddleware(
  state => state.nav,
  'root',
);

export default AppContainer;
