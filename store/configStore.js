import {createStore, combineReducers, compose, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';

import {navReducer} from '../navigations/screens';
import UIReducer from '../store/reducers/ui.reducer';
import ReqReducer from '../store/reducers/request.reducer';
import UserReducer from '../store/reducers/user.reducer';
import BookingUI from '../store/reducers/booking.reducer';
import BookingReducer from '../store/reducers/job.reducer';
import LocationReducer from '../store/reducers/location.reducer';

const rootReducers = combineReducers({
  navigation: navReducer,
  loading: UIReducer,
  response: ReqReducer,
  user: UserReducer,
  booking: BookingUI,
  job: BookingReducer,
  location: LocationReducer,
});

let composeEnhancers = compose;

if (__DEV__) {
  composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
}

const configureStore = () => {
  return createStore(rootReducers, composeEnhancers(applyMiddleware(thunk)));
};

export default configureStore;
