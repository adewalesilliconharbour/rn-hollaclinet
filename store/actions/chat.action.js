import axios from 'axios';
import api from '../../assets/connection';
import {Toast} from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import io from 'socket.io-client';
import {BOOKING_CHAT} from './actionTypes';

const socket = io(`${api.url}`);

const chats = payload => {
  return {
    type: BOOKING_CHAT,
    messages: payload,
  };
};

export const messages = payload => {
  return async _dispatch => {
    const sender = await AsyncStorage.getItem('id');
    await axios
      .get(
        `${api.url}${api.version}booking/chat/${payload.id}/${sender}/${payload.receciver}`,
      )
      .then(
        resp => {
          console.log(resp.data);
          const payload = resp.data;
          _dispatch(chats(payload));
        },
        err => {
          console.log(err.response);
        },
      );
  };
};
