import api from '../../assets/connection';
import AsyncStorage from '@react-native-community/async-storage';

export {userDetails, updateUserDetails} from './auth';

export const connection = {
  getUserDate: `${api.url}${api.version}user/`,
};
