import {
  REQUEST_UI,
  AWAIT_UI,
  ACCEPTED_UI,
  JOB_STATES,
  WORKER_LOCATION,
} from './actionTypes';

export const resquestWorker = () => {
  return {
    type: REQUEST_UI,
  };
};

export const requestResponse = () => {
  return {
    type: AWAIT_UI,
  };
};

export const inProgressState = state => {
  return {
    type: JOB_STATES,
    data: state,
  };
};

export const requestGoing = () => {
  return {
    type: ACCEPTED_UI,
  };
};

export const workersLocation = payload => {
  return {
    type: WORKER_LOCATION,
    location: payload,
  };
};
