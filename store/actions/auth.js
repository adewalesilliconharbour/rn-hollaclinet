import axios from 'axios';
import api from '../../assets/connection';
import {Toast} from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import {NavigationActions} from 'react-navigation';
import {
  isloading,
  stoploading,
  isSuccessfull,
  notSuccessfull,
  userInfo,
} from '../actions/index';

export const authSignUp = payload => {
  return async _dispatch => {
    _dispatch(isloading());
    await axios
      .post(`${api.url}${api.auth}${api.register}`, payload)
      .then(resp => {
        _dispatch(isSuccessfull());
        showToast(resp.data.message);
        _dispatch(stoploading());
      })
      .catch(err => {
        alert(err.response.data.message);
        _dispatch(notSuccessfull());
        _dispatch(stoploading());
      });
  };
};

export const authSignIn = payload => {
  return async _dispatch => {
    _dispatch(isloading());
    await axios
      .post(`${api.url}${api.auth}${api.login}`, payload)
      .then(resp => {
        showToast(resp.data.message);
        _dispatch(isSuccessfull());
        _dispatch(stoploading());
      })
      .catch(err => {
        alert(err.response.data.message);
        _dispatch(notSuccessfull());
        _dispatch(stoploading());
      });
  };
};

export const verifyUser = payload => {
  return async _dispatch => {
    _dispatch(isloading());
    await axios
      .post(`${api.url}${api.auth}${api.verifyOtp}`, payload)
      .then(async resp => {
        await AsyncStorage.setItem('auth_token', resp.data.token).then(res => {
          console.log(res);
        });
        await AsyncStorage.setItem('id', resp.data.user);
        _dispatch(stoploading());
        _dispatch(isSuccessfull());
      })
      .catch(err => {
        _dispatch(notSuccessfull());
        alert(err.response.data.message);
        _dispatch(stoploading());
      });
  };
};

export const initresponse = () => {
  return _dispatch => {
    _dispatch(notSuccessfull());
  };
};

export const loggedinUser = () => {
  return async _dispatch => {
    const user = await AsyncStorage.getItem('id');
    _dispatch(isloading());
    await axios
      .get(`${api.url}${api.version}user/${user}`)
      .then(resp => {
        const user = resp.data;
        _dispatch(userInfo(user));
        _dispatch(stoploading());
      })
      .catch(err => {
        console.log(err.response);
        _dispatch(stoploading());
      });
  };
};

export const userDetails = async () => {
  const user = await AsyncStorage.getItem('id');
  const data = await axios.get(`${api.url}${api.version}user/${user}`);
  return data;
};

export const updateProfile = payload => {
  return async _dispatch => {
    const user = await AsyncStorage.getItem('id');
    _dispatch(isloading());
    await axios
      .put(`${api.url}${api.version}user/${user}`, payload)
      .then(resp => {
        showToast(resp.data.success);
        _dispatch(isSuccessfull());
        setTimeout(() => {
          _dispatch(notSuccessfull());
        }, 500);
      })
      .catch(err => {
        console.log(err.response);
      });
  };
};

export const updateUserDetails = async payload => {
  const user = await AsyncStorage.getItem('id');
  const data = await axios.put(`${api.url}${api.version}user/${user}`, payload);
  return data;
};

const showToast = message => {
  Toast.show({
    text: message,
    buttonText: 'Okay',
    duration: 5000,
  });
};
