import { UI_START_LOADING, UI_STOP_LOADING } from './actionTypes';

export const isloading = () => {
    return {
        type: UI_START_LOADING,
    }
}

export const stoploading = () => {
    return {
        type: UI_STOP_LOADING
    }
}