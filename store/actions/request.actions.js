import {ERROR_RESPONSE, SUCCESS_RESPONSE} from '../actions/actionTypes';

export const isSuccessfull = () => {
  return {
    type: SUCCESS_RESPONSE,
  };
};

export const notSuccessfull = () => {
  return {
    type: ERROR_RESPONSE,
  };
};
