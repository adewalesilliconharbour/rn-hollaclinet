import axios from 'axios';
import api from '../../assets/connection';
import {Toast} from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import {
  isloading,
  isSuccessfull,
  notSuccessfull,
  stoploading,
  resquestWorker,
  requestResponse,
  inProgressState,
  requestGoing,
  workersLocation,
} from './index';
import io from 'socket.io-client';

const socket = io(`${api.url}`);

export const requestWorker = payload => {
  return async _dispatch => {
    _dispatch(isloading());
    const userid = await AsyncStorage.getItem('id');
    const request = {
      seekersId: userid,
      sender: payload.sender,
      skill: payload.skill,
      reqesting_for: {
        name: null,
        sex: null,
        phone: null,
      },
      jobLocation: payload.jobLocation,
      jobAddress: payload.jobAddress,
    };
    await axios.post(`${api.url}api/v1/booking/request/`, request).then(
      resp => {
        console.log(resp);
        _dispatch(requestResponse());
        socket.emit('refresh', {});
      },
      err => {
        console.log(err.response);
        alert(err.response.data.message);
        _dispatch(stoploading());
      },
    );
  };
};

export const availableJobs = () => {
  return async _dispatch => {
    const userid = await AsyncStorage.getItem('id');
    await axios
      .get(`${api.url}api/v1/booking/request/${userid}`)
      .then(resp => {
        let available = resp.data.length;
        if (available > 0) {
          let job = resp.data[0].status;
          job === 'pending'
            ? _dispatch(requestResponse())
            : _dispatch(requestGoing());
          _dispatch(inProgressState(resp.data[0]));
        } else {
          _dispatch(resquestWorker());
        }
      })
      .catch(err => {
        console.log(err.response.data);
      });
  };
};

export const alljobs = async () => {
  const userid = await AsyncStorage.getItem('id');
  const result = await axios.get(`${api.url}api/v1/booking/request/${userid}`);
  if (result) {
    return result;
  } else {
    return null;
  }
};

export const updateJobs = status => {
  return async _dispatch => {
    const userid = await AsyncStorage.getItem('id');
    await axios
      .patch(`${api.url}api/v1/booking/request/${userid}`, status)
      .then(
        resp => {
          console.log(resp);
          socket.emit('refresh', {});
        },
        err => {
          console.log(err.response);
          _dispatch(notSuccessfull());
        },
      );
  };
};

export const cancelJob = payload => {
  return async _dispatch => {
    const userid = await AsyncStorage.getItem('id');
    const data = {
      status: 'rejected',
      cancelReason: {
        canceller: userid,
        reason: payload.reason,
      },
    };
    await axios
      .put(`${api.url}api/v1/booking/request/${userid}/cancel`, data)
      .then(
        resp => {
          console.log(resp);
          socket.emit('refresh', {});
          _dispatch(NavigationActions.navigate({routeName: 'home'}));
        },
        err => {
          console.log(err.response);
        },
      );
  };
};

export const completeJob = () => {
  return async _dispatch => {
    const userid = await AsyncStorage.getItem('id');
    await axios.put(`${api.url}api/v1/booking/request/${userid}/complete`).then(
      resp => {
        console.log(resp);
        socket.emit('refresh', {});
        _dispatch(isSuccessfull());
        setTimeout(() => {
          _dispatch(notSuccessfull());
        }, 2000);
      },
      err => {
        console.log(err.response);
      },
    );
  };
};

export const reviewWorker = payload => {
  return async _dispatch => {
    const userid = await AsyncStorage.getItem('id');
    const rating_data = {
      user: userid,
      rating: payload.rating,
      comment: payload.comment,
    };
    await axios.post(`${api.url}api/v1/rating/${payload.id}`, rating_data).then(
      resp => {
        console.log(resp);
      },
      err => {
        console.log(err.response);
      },
    );
  };
};

export const agreeprice = payload => {
  return async _dispatch => {
    const userid = await AsyncStorage.getItem('id');
    const data = {
      method: payload.method,
    };
    await axios
      .put(`${api.url}api/v1/booking/request/${userid}/agreeprice`, data)
      .then(
        resp => {
          console.log(resp);
          _dispatch(isSuccessfull());
          socket.emit('refresh', {});
          setTimeout(() => {
            _dispatch(notSuccessfull());
          }, 2000);
        },
        err => {
          console.log(err.response);
        },
      );
  };
};

export const sendMessage = payload => {
  return async _dispatch => {
    const sender = await AsyncStorage.getItem('id');
    var data = {
      message: payload.message,
    };
    axios
      .post(
        `${api.url}api/v1/booking/chat/${payload.id}/${sender}/${payload.receciver}`,
        data,
      )
      .then(
        () => {
          socket.emit('refresh', {});
        },
        err => {
          console.log(err.response);
        },
      );
  };
};

export const workerLocation = () => {
  return async _dispatch => {
    await axios
      .get(`${api.url}api/v1/user/location/worker/Location`)
      .then(resp => {
        _dispatch(workersLocation(resp.data));
      })
      .catch(err => {
        console.log(err.response);
      });
  };
};

// const interface = job => {
//   switch (job) {
//     case 'pending':
//       _dispatch(requestResponse());
//     default:
//       _dispatch(requestProgress());
//   }
// };
