import {USER_DATA} from './actionTypes';

export const userInfo = payload => {
  return {
    type: USER_DATA,
    user: payload,
  };
};
