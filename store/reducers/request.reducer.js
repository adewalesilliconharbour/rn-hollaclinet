import {ERROR_RESPONSE, SUCCESS_RESPONSE} from '../actions/actionTypes';

initialstate = {
  isSuccess: false,
};

const reducer = (state = initialstate, action) => {
  switch (action.type) {
    case SUCCESS_RESPONSE:
      return {
        isSuccess: true,
      };
    case ERROR_RESPONSE:
      return {
        isSuccess: false,
      };
    default:
      return state;
  }
};

export default reducer;
