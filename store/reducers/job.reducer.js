import {JOB_STATES} from '../actions/actionTypes';

inistate = {
  status: '',
  id: '',
  worker: {
    id: '',
    firstName: '',
    lastName: '',
    verified: false,
    avatar: '',
    phone: '',
    countryCode: '',
    review: '',
  },
  payment: {},
  conversation: [],
  address: '',
};

const reducer = (state = inistate, action) => {
  switch (action.type) {
    case JOB_STATES:
      const details = action.data;
      workerState = {
        status: details.status,
        id: details._id,
        worker: {
          id: details.workersId._id,
          firstName: details.workersId.firstName,
          lastName: details.workersId.lastName,
          verified: details.workersId.isConfirmed,
          avatar: details.workersId.avatar,
          phone: details.workersId.phone,
          countryCode: details.workersId.country_code,
          lastName: details.workersId.lastName,
          review: details.workersId.reviews.length == 0 ? 'New Member' : '5.0',
        },
        payment: details.payment,
        conversation: details.conversation,
        address: details.jobAddress,
      };
      return {
        workerState,
      };
    default:
      return state;
  }
};

export default reducer;
