import {REQUEST_UI, AWAIT_UI, ACCEPTED_UI} from '../actions/actionTypes';

initialstate = {
  status: 'request',
};

const reducer = (state = initialstate, action) => {
  switch (action.type) {
    case AWAIT_UI:
      return {
        status: 'awaiting',
      };
    case ACCEPTED_UI:
      return {
        status: 'accepted',
      };
    case REQUEST_UI:
      return {
        status: 'request',
      };
    default:
      return state;
  }
};

export default reducer;
