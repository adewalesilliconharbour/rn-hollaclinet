import { UI_START_LOADING, UI_STOP_LOADING } from '../actions/actionTypes';

const initialState = {
    isloading: false
};

const reducer = (state = initialState, actions) => {
    switch (actions.type) {
        case UI_START_LOADING:
            return {
               ...state, isloading: true
            }
        case UI_STOP_LOADING:
            return {
               ...state, isloading: false
            }
        default:
            return state
    }
}

export default reducer