import {USER_DATA} from '../actions/actionTypes';

initialState = {
  avatar: '',
  firstName: '',
  lastName: '',
  email: '',
  sex: '',
  phone: '',
  countryCode: '',
  date_joined: '',
  verification: false,
  refCode: '',
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case USER_DATA:
      const user = {
        avatar: action.user.avatar,
        firstName: action.user.firstName,
        lastName: action.user.lastName,
        email: action.user.email,
        sex: action.user.sex,
        phone: action.user.phone,
        countryCode: action.user.country_code,
        verification: action.user.isConfirmed,
        date_joined: action.user.created_date,
        refCode: action.user.refcode,
      };
      return {
        user,
      };
    default:
      return state;
  }
};

export default reducer;
