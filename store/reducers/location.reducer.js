import {BOOKING_CHAT, WORKER_LOCATION} from '../actions/actionTypes';

initState = {
  user: [],
};

const reducer = (state = initState, action) => {
  switch (action.type) {
    case WORKER_LOCATION:
      return {
        ...state,
        user: action.location,
      };
    default:
      return state;
  }
};

export default reducer;
