/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

// import React from 'react';
import AppWithNavigationState from './navigations/screens';
import {connect} from 'react-redux';
import {Root, Text, View, Icon} from 'native-base';
import AppIntroSlider from 'react-native-app-intro-slider';
import slides from './assets/sliders';

import React, {Component} from 'react';
import {StatusBar, Image, StyleSheet} from 'react-native';
import {Button} from 'react-native-elements';

export class App extends Component {
  render() {
    return (
      <Root>
        <AppWithNavigationState />
      </Root>
    );
  }
}

const mapStateToProps = state => {
  return {
    navigation: state.navigation,
  };
};

const style = StyleSheet.create({});

export default connect(mapStateToProps)(App);
