import React, {Component} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  Modal,
  Image,
  Alert,
  Linking,
  ScrollView,
  Platform,
} from 'react-native';
import {
  Container,
  Header,
  Left,
  Body,
  Title,
  Right,
  Separator,
} from 'native-base';
import Geolocation from 'react-native-geolocation-service';
import {
  Divider,
  Input,
  ListItem,
  Avatar,
  Button,
  Icon,
} from 'react-native-elements';
import Maps from '../components/Maps';
import AsyncStorage from '@react-native-community/async-storage';
import {connect} from 'react-redux';
import {
  loggedinUser,
  requestWorker,
  availableJobs,
  messages,
  updateJobs,
} from '../store/actions/index';
import SlidingUpPanel from 'rn-sliding-up-panel';
import location from '../assets/location.png';
import rating from '../assets/rating.png';
const {height} = Dimensions.get('window');
import api from '../assets/connection';
import io from 'socket.io-client';
import wallet from '../assets/wallet.png';
import ticket from '../assets/ticket.png';
import close from '../assets/icons-close.png';
import distance from '../assets/icons-Distance.png';
import Geocoder from 'react-native-geocoding';
import {skill} from '../assets/api';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
import {alljobs} from '../store/actions/booking';
import {connection} from '../store/actions/new_index';
import axios from 'axios';

export class Home extends Component {
  longitude;
  latitude;

  constructor(props) {
    super(props);
    this.state = {
      user: {},
      jobs: {},
      skill: '',
      lng: '',
      lat: '',
      jobAddress: '',
      modalVisible: false,
      acceptedModel: false,
      Search: '',
      result: skill,
      allskill: skill,
    };
    Geocoder.init(api.key);
  }
  componentDidMount() {
    console.log(this.props.navigation);
    this.socket = io(`${api.url}`);
    this._myLocation();
    // this._userData();
    this.props.userData();
    this._availableJobs();
    this.props.availablejobs();
    this.socket.on('refreshpage', () => {
      this.props.userData();
      this.props.availablejobs();
    });
  }

  // getting user's data from database
  _userData = async () => {
    const user = await AsyncStorage.getItem('id');
    await axios.get(`${connection.getUserDate}${user}`).then(
      resp => {
        this.setState({user: resp.data});
      },
      err => {
        console.log(err.response);
      },
    );
  };

  // getting available jobs from database
  _availableJobs = async () => {
    const user = await AsyncStorage.getItem('id');
    await axios.get(`${api.url}api/v1/booking/request/${user}`).then(
      resp => {
        this.setState({jobs: resp.data});
      },
      err => {
        console.log(err.response);
      },
    );
  };

  _myLocation = () => {
    Geolocation.getCurrentPosition(
      res => {
        (this.longitude = res.coords.longitude),
          (this.latitude = res.coords.latitude);
        this.setState({
          lng: res.coords.longitude,
          lat: res.coords.latitude,
        });
        Geocoder.from(res.coords.latitude, res.coords.longitude).then(
          data => {
            const address = data.results[0].formatted_address;
            this.setState({jobAddress: address});
          },
          err => {
            console.log(err);
          },
        );
      },
      err => {
        console.log(err);
      },
    );
  };

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  setSearchText(query) {
    this.setState({Search: query});
    var results = skill.filter(
      el => el.toLowerCase().indexOf(query.toLowerCase()) !== -1,
    );
    this.setState({
      result: results,
    });
  }

  request() {
    const {requesting, user} = this.props;
    var request = {
      sender: `${user.firstName} ${user.lastName}`,
      skill: this.state.skill,
      jobLocation: {
        coordinates: [this.state.lng, this.state.lat],
      },
      jobAddress: this.state.jobAddress,
    };
    requesting(request);
  }

  _callWorker = jobs => {
    let phoneNumber = '';
    if (Platform.OS === 'android') {
      phoneNumber = 'tel:${' + jobs.worker.phone + '}';
    } else {
      phoneNumber = 'telprompt:${' + jobs.worker.phone + '}';
    }
    Linking.openURL(phoneNumber);
    this.props.navigation.navigate('confirm');
  };

  _jobstatus = status => {
    const {editstatus} = this.props;
    const payload = {
      status: status,
    };
    editstatus(payload);
  };

  bookingUI(state) {
    switch (state) {
      case 'request':
        const {result} = this.state;
        var skills = result.map((data, i) => (
          <ListItem
            leftAvatar={
              <Avatar
                key={i}
                rounded
                overlayContainerStyle={{backgroundColor: '#fff'}}
                size="medium"
                icon={{name: 'person', color: 'red'}}
              />
            }
            containerStyle={{paddingVertical: 0}}
            bottomDivider
            title={data}
            onPress={() => {
              this.setState({skill: data, Search: data});
              setTimeout(() => {
                this._panel.hide();
                this.request();
              }, 2000);
            }}
          />
        ));
        return (
          <SlidingUpPanel
            ref={c => (this._panel = c)}
            backdropOpacity={0.4}
            animatedValue={this._draggedValue}
            // friction={3}

            containerStyle={{
              shadowRadius: 12,
              shadowOffset: {width: 1, height: 1},
              shadowOpacity: 0.3,
            }}
            draggableRange={{top: height / 1.2, bottom: 250}}>
            <View
              style={{
                backgroundColor: 'white',
                // height: 300,
                borderTopRightRadius: 24,
                borderTopLeftRadius: 24,
              }}>
              <Divider
                style={{
                  height: 6,
                  borderRadius: 20,
                  marginTop: 10,
                  marginHorizontal: '42%',
                }}
              />
              <View style={{paddingHorizontal: 10}}>
                <View style={{flexDirection: 'row', marginTop: 40}}>
                  <View style={{width: 42}}>
                    <Image
                      source={location}
                      style={{
                        width: 30,
                        bottom: 110,
                        left: 10,
                        resizeMode: 'contain',
                      }}
                    />
                  </View>
                  <View style={{flex: 1}}>
                    <TouchableOpacity
                      onPress={() => this.setModalVisible(true)}>
                      <Input
                        containerStyle={{marginBottom: 15}}
                        label="JOB LOCATION"
                        labelStyle={{fontSize: 14, fontWeight: '500'}}
                        inputStyle={{fontSize: 13}}
                        placeholder="Loading....."
                        value={this.state.jobAddress}
                        disabled
                      />
                    </TouchableOpacity>
                    <TouchableOpacity
                      onPress={() => {
                        this._panel.show();
                      }}>
                      <Input
                        label="SEARCH FOR WORKER"
                        labelStyle={{fontSize: 14, fontWeight: '500'}}
                        inputStyle={{fontSize: 13}}
                        onChangeText={text => this.setSearchText(text)}
                        value={this.state.Search}
                        placeholder="e.g plumber, carpenter, bricklayer, etc"
                        clearButtonMode="always"
                      />
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
              <View style={{top: -170}}>
                <ListItem
                  containerStyle={{backgroundColor: '#EFEFEF'}}
                  title=""
                />
                <ListItem
                  titleStyle={{color: '#C8C7CC'}}
                  title="POPULAR/RECENT JOBS"
                />
                <ScrollView style={{height: '100%'}}>
                  <View>{skills}</View>
                </ScrollView>
              </View>
            </View>
          </SlidingUpPanel>
        );
      case 'awaiting':
        return (
          <View style={{position: 'absolute', bottom: 0, width: '100%'}}>
            <View style={{height: '100%', backgroundColor: '#358DCE'}}>
              <Divider
                style={{
                  height: 6,
                  borderRadius: 20,
                  marginTop: 10,
                  marginHorizontal: '42%',
                  backgroundColor: '#fff',
                }}
              />
              <Text
                style={{
                  textAlign: 'center',
                  marginVertical: 15,
                  fontSize: 15,
                  color: '#fff',
                }}>
                Connecting you to nearby workers
              </Text>
              <Button
                buttonStyle={{
                  backgroundColor: '#fff',
                  marginHorizontal: 50,
                  marginBottom: 50,
                }}
                titleStyle={{color: '#000'}}
                title="Cancel"
              />
            </View>
          </View>
        );
      case 'accepted':
        const {jobs, navigation} = this.props;
        let info;
        let modal;
        let actions;
        switch (jobs ? jobs.status : null) {
          case 'accepted':
            // Alert.alert(
            //   `${jobs.worker.firstName} ${jobs.worker.lastName}`,
            //   'Your request has been accepted by one of our available workers',
            //   [
            //     {text: 'Call', onPress: () => this._callWorker(jobs)},
            //     {text: 'Chat', onPress: () => navigation.push('chat')},
            //   ],
            //   {cancelable: false},
            // );
            // console.log(Alert.prompt);
            // modal = (
            //   <View>
            //     <Modal transparent visible={(this.state.acceptedModel = true)}>
            //       <View
            //         style={{
            //           backgroundColor: 'rgba(0,0,0,0.5)',
            //           height: '100%',
            //           justifyContent: 'center',
            //           flexDirection: 'column',
            //           flex: 1,
            //         }}>
            //
            //       </View>
            //     </Modal>
            //   </View>
            // );
            info = (
              <View
                style={{
                  alignSelf: 'center',
                  padding: 20,
                  alignItems: 'center',
                }}>
                <Avatar rounded icon={{name: 'person'}} size="large" />
                <Text style={{marginTop: 10, marginBottom: 10, fontSize: 20}}>
                  {`${jobs.worker.firstName} ${jobs.worker.lastName}`}
                </Text>
                <Text style={{marginHorizontal: 10, textAlign: 'center'}}>
                  Your request has been accepted by one of our available workers
                </Text>
                <View
                  style={{
                    flexDirection: 'row',
                    marginTop: 20,
                  }}>
                  <Button
                    containerStyle={{flex: 1}}
                    buttonStyle={{backgroundColor: '#4CD964'}}
                    titleStyle={{fontWeight: '800'}}
                    title="Call"
                    icon={{name: 'call', color: '#fff'}}
                    onPress={() => this._callWorker(jobs)}
                  />
                  <View style={{marginHorizontal: 10}}></View>
                  <Button
                    containerStyle={{flex: 1}}
                    buttonStyle={{backgroundColor: '#358DCE'}}
                    title="Chat"
                    titleStyle={{fontWeight: '800'}}
                    icon={{name: 'chat', color: '#fff'}}
                    onPress={() => navigation.push('chat')}
                  />
                </View>
              </View>
            );
            // info = (
            //   <Text
            //     style={{
            //       paddingHorizontal: 20,
            //       paddingVertical: 10,
            //       fontSize: 17,
            //       textAlign: 'center',
            //       color: '#FF0200',
            //     }}>
            //     {jobs
            //       ? `${jobs.worker.firstName} ${jobs.worker.lastName} has accepted your job request`
            //       : 'loading.....'}
            //   </Text>
            // );
            actions = (
              <Button
                containerStyle={{
                  marginVertical: 18,
                  paddingHorizontal: 12,
                }}
                buttonStyle={{borderRadius: 10}}
                title="Waiting for worker....."
                disabled
                onPress={() => null}
              />
            );
            break;
          case 'enrouted':
            info = (
              <Text
                style={{
                  paddingHorizontal: 20,
                  paddingVertical: 10,
                  fontSize: 17,
                  textAlign: 'center',
                  color: '#FF0200',
                }}>
                {jobs
                  ? `${jobs.worker.firstName} ${jobs.worker.lastName} is on his way`
                  : 'loading.....'}
              </Text>
            );
            actions = (
              <Button
                containerStyle={{
                  marginVertical: 18,
                  paddingHorizontal: 12,
                }}
                buttonStyle={{borderRadius: 10}}
                title="Waiting for worker....."
                disabled
                onPress={() => null}
              />
            );
            break;
          case 'arrived':
            info = (
              <Text
                style={{
                  paddingHorizontal: 20,
                  paddingVertical: 10,
                  fontSize: 17,
                  textAlign: 'center',
                  color: '#FF0200',
                }}>
                {jobs
                  ? `${jobs.worker.firstName} ${jobs.worker.lastName} has arrived your requested job location`
                  : 'loading.....'}
              </Text>
            );
            actions = (
              <Button
                containerStyle={{
                  marginVertical: 18,
                  paddingHorizontal: 12,
                }}
                buttonStyle={{borderRadius: 10}}
                title="CONFIRM ARRIVAL"
                titleStyle={{fontWeight: '600'}}
                onPress={() => this._jobstatus('atLocation')}
              />
            );
            break;
          case 'atLocation':
            info = (
              <Text
                style={{
                  paddingHorizontal: 20,
                  paddingVertical: 10,
                  fontSize: 17,
                  textAlign: 'center',
                  color: '#FF0200',
                }}>
                {jobs
                  ? `Please negotiate with ${jobs.worker.firstName} ${jobs.worker.lastName} and accept his billing`
                  : 'loading.....'}
              </Text>
            );
            actions = (
              <Button
                containerStyle={{
                  marginVertical: 18,
                  paddingHorizontal: 12,
                }}
                buttonStyle={{borderRadius: 10}}
                title="NEGOTIATE PRICE"
                titleStyle={{fontWeight: '600'}}
                disabled
                // onPress={() => this._jobstatus('atLocation')}
              />
            );
            break;
          case 'negotiate':
            info = (
              <Text
                style={{
                  paddingHorizontal: 20,
                  paddingVertical: 10,
                  fontSize: 17,
                  textAlign: 'center',
                  color: '#FF0200',
                }}>
                {jobs
                  ? `Please check what ${jobs.worker.firstName} ${jobs.worker.lastName} has billed you`
                  : 'loading.....'}
              </Text>
            );
            actions = (
              <Button
                containerStyle={{
                  marginVertical: 18,
                  paddingHorizontal: 12,
                }}
                buttonStyle={{borderRadius: 10}}
                title="VIEW PRICE"
                titleStyle={{fontWeight: '600'}}
                onPress={() => navigation.navigate('price')}
              />
            );
            break;
          case 'startjob':
            info = (
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-around',
                }}>
                <TouchableOpacity>
                  <View style={{marginVertical: 13}}>
                    <Image
                      style={{
                        width: '70%',
                        left: '15%',
                        marginBottom: 9,
                      }}
                      source={wallet}
                    />
                    <Text
                      style={{
                        fontSize: 15,
                        color: '#8A8A8F',
                        textAlign: 'center',
                      }}>
                      Payment
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity>
                  <View style={{marginVertical: 13}}>
                    <Image
                      style={{
                        width: '50%',
                        left: '25%',
                        marginBottom: 9,
                      }}
                      source={ticket}
                    />
                    <Text style={{fontSize: 15, color: '#8A8A8F'}}>
                      Promo code
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity>
                  <View style={{marginVertical: 13}}>
                    <Image
                      style={{
                        width: '60%',
                        left: '20%',
                        marginBottom: 9,
                      }}
                      source={close}
                    />
                    <Text style={{fontSize: 15, color: '#8A8A8F'}}>Cancel</Text>
                  </View>
                </TouchableOpacity>
              </View>
            );
            actions = (
              <Button
                containerStyle={{
                  marginVertical: 18,
                  paddingHorizontal: 12,
                }}
                buttonStyle={{borderRadius: 10}}
                disabled
                title="Waiting for worker to start job...."
                titleStyle={{fontWeight: '600'}}
                onPress={() => navigation.navigate('price')}
              />
            );
            break;
          case 'confirmjob':
            info = (
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-around',
                }}>
                <TouchableOpacity>
                  <View style={{marginVertical: 13}}>
                    <Image
                      style={{
                        width: '70%',
                        left: '15%',
                        marginBottom: 9,
                      }}
                      source={wallet}
                    />
                    <Text
                      style={{
                        fontSize: 15,
                        color: '#8A8A8F',
                        textAlign: 'center',
                      }}>
                      Payment
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity>
                  <View style={{marginVertical: 13}}>
                    <Image
                      style={{
                        width: '50%',
                        left: '25%',
                        marginBottom: 9,
                      }}
                      source={ticket}
                    />
                    <Text style={{fontSize: 15, color: '#8A8A8F'}}>
                      Promo code
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity>
                  <View style={{marginVertical: 13}}>
                    <Image
                      style={{
                        width: '60%',
                        left: '20%',
                        marginBottom: 9,
                      }}
                      source={close}
                    />
                    <Text style={{fontSize: 15, color: '#8A8A8F'}}>Cancel</Text>
                  </View>
                </TouchableOpacity>
              </View>
            );
            actions = (
              <Button
                containerStyle={{
                  marginVertical: 18,
                  paddingHorizontal: 12,
                }}
                buttonStyle={{borderRadius: 10}}
                title="CONFIRM JOB START"
                titleStyle={{fontWeight: '600'}}
                onPress={() => this._jobstatus('inprogress')}
              />
            );
            break;
          case 'inprogress':
            info = (
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-around',
                }}>
                <TouchableOpacity>
                  <View style={{marginVertical: 13}}>
                    <Image
                      style={{
                        width: '70%',
                        left: '15%',
                        marginBottom: 9,
                      }}
                      source={wallet}
                    />
                    <Text
                      style={{
                        fontSize: 15,
                        color: '#8A8A8F',
                        textAlign: 'center',
                      }}>
                      Payment
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity>
                  <View style={{marginVertical: 13}}>
                    <Image
                      style={{
                        width: '50%',
                        left: '25%',
                        marginBottom: 9,
                      }}
                      source={ticket}
                    />
                    <Text style={{fontSize: 15, color: '#8A8A8F'}}>
                      Promo code
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity>
                  <View style={{marginVertical: 13}}>
                    <Image
                      style={{
                        width: '60%',
                        left: '20%',
                        marginBottom: 9,
                      }}
                      source={close}
                    />
                    <Text style={{fontSize: 15, color: '#8A8A8F'}}>Cancel</Text>
                  </View>
                </TouchableOpacity>
              </View>
            );
            actions = (
              <Button
                containerStyle={{
                  marginVertical: 18,
                  paddingHorizontal: 12,
                }}
                buttonStyle={{borderRadius: 10}}
                disabled
                title="Job in progress please wait...."
                titleStyle={{fontWeight: '600'}}
                onPress={() => null}
              />
            );
            break;
          case 'job end':
            info = (
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-around',
                }}>
                <TouchableOpacity>
                  <View style={{marginVertical: 13}}>
                    <Image
                      style={{
                        width: '70%',
                        left: '15%',
                        marginBottom: 9,
                      }}
                      source={wallet}
                    />
                    <Text
                      style={{
                        fontSize: 15,
                        color: '#8A8A8F',
                        textAlign: 'center',
                      }}>
                      Payment
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity>
                  <View style={{marginVertical: 13}}>
                    <Image
                      style={{
                        width: '50%',
                        left: '25%',
                        marginBottom: 9,
                      }}
                      source={ticket}
                    />
                    <Text style={{fontSize: 15, color: '#8A8A8F'}}>
                      Promo code
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity>
                  <View style={{marginVertical: 13}}>
                    <Image
                      style={{
                        width: '60%',
                        left: '20%',
                        marginBottom: 9,
                      }}
                      source={close}
                    />
                    <Text style={{fontSize: 15, color: '#8A8A8F'}}>Cancel</Text>
                  </View>
                </TouchableOpacity>
              </View>
            );
            actions = (
              <Button
                containerStyle={{
                  marginVertical: 18,
                  paddingHorizontal: 12,
                }}
                buttonStyle={{borderRadius: 10}}
                title="CONFIRM JOB ENDS"
                titleStyle={{fontWeight: '600'}}
                onPress={() => navigation.navigate('review')}
              />
            );
        }

        return (
          <View
            style={{
              position: 'absolute',
              width: '94%',
              left: '3%',
              right: '3%',
              bottom: 30,
            }}>
            <View
              style={{
                backgroundColor: '#fff',
                borderRadius: 14,
                shadowOpacity: 0.5,
                shadowRadius: 30,
              }}>
              {jobs ? (
                jobs.status != 'accepted' ? (
                  <ListItem
                    leftAvatar={{
                      icon: {name: 'home'},
                      size: 'medium',
                    }}
                    rightElement={
                      <Avatar
                        overlayContainerStyle={{backgroundColor: '#4CE5B1'}}
                        rounded
                        icon={{name: 'phone'}}
                        size={43}
                        onPress={() => this._callWorker(jobs)}
                      />
                    }
                    rightAvatar={
                      <Avatar
                        overlayContainerStyle={{backgroundColor: '#4252FF'}}
                        rounded
                        icon={{name: 'chat'}}
                        size={43}
                        onPress={() => navigation.push('chat')}
                      />
                    }
                    containerStyle={{
                      backgroundColor: '#F7F7F7',
                      borderTopRightRadius: 14,
                      borderTopLeftRadius: 14,
                    }}
                    titleProps={{numberOfLines: 1}}
                    titleStyle={{fontSize: 19}}
                    title={
                      jobs
                        ? `${jobs.worker.firstName} ${jobs.worker.lastName}`
                        : 'loading....'
                    }
                    subtitle={
                      <Text style={{fontSize: 15, color: '#C8C7CC'}}>
                        <Image source={rating} />{' '}
                        {jobs ? jobs.worker.review : 'loading'}
                      </Text>
                    }
                  />
                ) : null
              ) : null}
              {info}
              {jobs ? (
                jobs.status != 'accepted' ? (
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-around',
                      marginVertical: 14,
                    }}>
                    <View>
                      <Image source={distance} />
                    </View>
                    <View>
                      <Text
                        style={{
                          textAlign: 'center',
                          fontSize: 13,
                          fontWeight: '700',
                          color: '#C8C7CC',
                          marginBottom: 9,
                        }}>
                        DISTANCE
                      </Text>
                      <Text
                        style={{
                          textAlign: 'center',
                          fontSize: 15,
                          fontWeight: '800',
                        }}>
                        0.2 km
                      </Text>
                    </View>
                    <View>
                      <Text
                        style={{
                          textAlign: 'center',
                          fontSize: 13,
                          fontWeight: '700',
                          color: '#C8C7CC',
                          marginBottom: 9,
                        }}>
                        TIME
                      </Text>
                      <Text
                        style={{
                          textAlign: 'center',
                          fontSize: 15,
                          fontWeight: '800',
                        }}>
                        2 min
                      </Text>
                    </View>
                    <View>
                      <Text
                        style={{
                          textAlign: 'center',
                          fontSize: 13,
                          fontWeight: '700',
                          color: '#C8C7CC',
                          marginBottom: 9,
                        }}>
                        CALL OUT FEE
                      </Text>
                      <Text
                        style={{
                          textAlign: 'center',
                          fontSize: 15,
                          fontWeight: '800',
                        }}>
                        ₦500.00
                      </Text>
                    </View>
                  </View>
                ) : null
              ) : null}
              {actions}
            </View>
          </View>
        );
      default:
        return null;
    }
  }

  render() {
    const {booking, navigation, jobs} = this.props;
    const {user} = this.state;
    const homePlace = {
      description: 'My Current Location',
      geometry: {
        location: {
          lat: this.latitude,
          lng: this.longitude,
        },
      },
    };
    return (
      <Container>
        <View>
          <Modal
            animationType="fade"
            presentationStyle="formSheet"
            transparent={false}
            visible={this.state.modalVisible}>
            <View>
              <View>
                <Header>
                  <Left>
                    <Button
                      type="clear"
                      title="Cancel"
                      onPress={() =>
                        this.setModalVisible(!this.state.modalVisible)
                      }
                    />
                  </Left>
                  <Body>
                    <Title>Set Location</Title>
                  </Body>
                  <Right></Right>
                </Header>
                <GooglePlacesAutocomplete
                  placeholder="Enter Location"
                  minLength={2}
                  autoFocus={false}
                  returnKeyType={'default'}
                  fetchDetails={true}
                  query={{key: `${api.key}`}}
                  listViewDisplayed="auto"
                  renderDescription={row => row.description}
                  onPress={(data, details = null) => {
                    this.setState({
                      lat: details.geometry.location.lat,
                      lng: details.geometry.location.lng,
                      jobAddress: details.formatted_address,
                      modalVisible: false,
                    });
                    console.log(
                      // this.setModalVisible(!this.state.modalVisible)
                      details.geometry.location,
                      details.formatted_address,
                    );
                  }}
                  // GooglePlacesDetailsQuery={{
                  //   fields: 'formatted_address',
                  // }}
                  getDefaultValue={() => ''}
                  nearbyPlacesAPI="GoogleMapsPlacesSearch"
                  GoogleMapsPlacesSearchQuery={{
                    rankby: 'distance',
                    type: 'cafe',
                  }}
                  predefinedPlaces={[homePlace]}
                  styles={{
                    textInputContainer: {
                      backgroundColor: 'rgba(0,0,0,0)',
                      borderTopWidth: 0,
                      borderBottomWidth: 0,
                    },
                    // textInput: {
                    //   marginLeft: 0,
                    //   marginRight: 0,
                    //   height: 38,
                    //   color: '#5d5d5d',
                    //   fontSize: 16,
                    // },
                    predefinedPlacesDescription: {
                      color: '#000',
                    },
                    listView: {
                      top: 40,
                      position: 'absolute',
                      height: Dimensions.get('window').width,
                      width: Dimensions.get('window').width,
                    },
                  }}
                  currentLocation={false}>
                  {/* <ListItem title={row.description} /> */}
                </GooglePlacesAutocomplete>
              </View>
            </View>
          </Modal>
        </View>
        <View>
          <Maps height={520} />
          {this.bookingUI(booking)}
          <View style={{position: 'absolute', top: 0}}>
            <Header style={{marginTop: 15}} transparent>
              <Left>
                <Icon
                  reverse
                  raised
                  name="align-left"
                  type="feather"
                  color="#000"
                  size={25}
                  onPress={() => navigation.openDrawer()}
                />
              </Left>
            </Header>
          </View>
        </View>
      </Container>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    userData: () => dispatch(loggedinUser()),
    availablejobs: () => dispatch(availableJobs()),
    requesting: payload => dispatch(requestWorker(payload)),
    editstatus: payload => dispatch(updateJobs(payload)),
    chats: payload => dispatch(messages(payload)),
  };
};

const mapStateToProps = state => {
  return {
    booking: state.booking.status,
    user: state.user.user,
    jobs: state.job.workerState,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
