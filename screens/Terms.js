import React, {Component} from 'react';
import {View, ScrollView} from 'react-native';
import terms from '../assets/terms';
import {Header, Text, Title} from 'native-base';
import {SafeAreaView} from 'react-native-safe-area-context';

export class Terms extends Component {
  render() {
    return (
      <View>
        <Header>
          <Title>Terms & Privacy Policy</Title>
        </Header>
        <Text
          style={{
            marginTop: 20,
            textAlign: 'center',
            fontWeight: '700',
            fontSize: 20,
          }}>
          Terms and conditions
        </Text>
        <View style={{paddingHorizontal: 22, marginBottom: 300}}>
          <ScrollView>
            <Text style={{textAlign: 'justify'}}>
              {terms.Termsandconditions}
            </Text>
            <Text style={{fontWeight: '800', marginVertical: 20, fontSize: 17}}>
              Accounts and membership
            </Text>
            <Text style={{textAlign: 'justify'}}>
              {terms.Accountsandmembership}
            </Text>
            <Text style={{fontWeight: '800', marginVertical: 20, fontSize: 17}}>
              Backups
            </Text>
            <Text style={{textAlign: 'justify'}}>{terms.Backups}</Text>
            <Text style={{fontWeight: '800', marginVertical: 20, fontSize: 17}}>
              Links to other mobile applications
            </Text>
            <Text style={{textAlign: 'justify'}}>
              {terms.Linkstoothermobileapplications}
            </Text>
            <Text style={{fontWeight: '800', marginVertical: 20, fontSize: 17}}>
              Prohibited uses
            </Text>
            <Text style={{textAlign: 'justify'}}>{terms.Prohibiteduses}</Text>
            <Text style={{fontWeight: '800', marginVertical: 20, fontSize: 17}}>
              Intellectual property rights
            </Text>
            <Text style={{textAlign: 'justify'}}>
              {terms.Intellectualpropertyrights}
            </Text>
            <Text style={{fontWeight: '800', marginVertical: 20, fontSize: 17}}>
              Limitation of liability
            </Text>
            <Text style={{textAlign: 'justify'}}>
              {terms.Limitationofliability}
            </Text>
            <Text style={{fontWeight: '800', marginVertical: 20, fontSize: 17}}>
              Indemnification
            </Text>
            <Text style={{textAlign: 'justify'}}>{terms.Indemnification}</Text>
            <Text style={{fontWeight: '800', marginVertical: 20, fontSize: 17}}>
              Severability
            </Text>
            <Text style={{textAlign: 'justify'}}>{terms.Severability}</Text>
            <Text style={{fontWeight: '800', marginVertical: 20, fontSize: 17}}>
              Dispute resolution
            </Text>
            <Text style={{textAlign: 'justify'}}>
              {terms.Disputeresolution}
            </Text>
            <Text style={{fontWeight: '800', marginVertical: 20, fontSize: 17}}>
              Changes and amendments
            </Text>
            <Text style={{textAlign: 'justify'}}>
              {terms.Changesandamendments}
            </Text>
            <Text style={{fontWeight: '800', marginVertical: 20, fontSize: 17}}>
              Contacting us
            </Text>
            <Text style={{textAlign: 'justify'}}>{terms.Contactingus}</Text>
          </ScrollView>
        </View>
      </View>
    );
  }
}

export default Terms;
