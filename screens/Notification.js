import React, {Component} from 'react';
import {Text, View} from 'react-native';
import HeaderNav from '../components/Header';
import {ListItem, Avatar} from 'react-native-elements';
import {NotificationList} from '../assets/api';
import {ScrollView} from 'react-native-gesture-handler';

export class Notification extends Component {
  render() {
    const {navigation} = this.props;
    var notify = NotificationList.map(data => (
      <ListItem
        leftAvatar={
          <Avatar
            rounded
            overlayContainerStyle={{backgroundColor: '#000000'}}
            size="medium"
            icon={{name: 'home', color: '#fff'}}
          />
        }
        containerStyle={{paddingVertical: 20}}
        bottomDivider
        title={data.type}
        subtitleProps={{numberOfLines: 1}}
        subtitle={data.content}
      />
    ));
    return (
      <View>
        <HeaderNav title="Notification" navigation={navigation} />
        <ScrollView>{notify}</ScrollView>
      </View>
    );
  }
}

export default Notification;
