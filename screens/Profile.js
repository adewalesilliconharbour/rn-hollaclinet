import React, {Component} from 'react';
import {Text, View} from 'react-native';
import {connect} from 'react-redux';
import HeaderNav from '../components/Header';
import {Separator, Container} from 'native-base';
import Moment from 'react-moment';
import {ListItem} from 'react-native-elements';

export class Profile extends Component {
  render() {
    const {user, navigation} = this.props;
    return (
      <Container>
        <HeaderNav title="My Account" navigation={navigation} />
        <Separator />
        <ListItem
          title="Verification"
          rightTitleStyle={{width: 250, textAlign: 'right'}}
          rightTitle={user.verification ? 'Verified member' : 'Not Verified'}
          chevron
        />
        <Separator />
        <ListItem
          title="Name"
          rightTitle={`${user.firstName} ${user.lastName}`}
          rightTitleProps={{numberOfLines: 1}}
          rightTitleStyle={{width: 250, textAlign: 'right'}}
          chevron
          onPress={() => navigation.navigate('editProfile')}
          bottomDivider
        />
        <ListItem
          title="Email"
          rightTitle={user.email}
          rightTitleProps={{numberOfLines: 1}}
          rightTitleStyle={{width: 250, textAlign: 'right'}}
          chevron
          bottomDivider
        />
        <ListItem
          title="Sex"
          rightTitle={user.sex.replace(/^\w/, c => c.toUpperCase())}
          rightTitleProps={{numberOfLines: 1}}
          rightTitleStyle={{width: 250, textAlign: 'right'}}
          chevron
          bottomDivider
        />
        <ListItem
          title="Phone number"
          rightTitleProps={{numberOfLines: 1}}
          rightTitle={`${user.countryCode} ${user.phone}`}
          rightTitleStyle={{width: 250, textAlign: 'right'}}
          chevron
          bottomDivider
        />
        <ListItem
          title="Member since"
          rightTitle={
            <Moment
              element={Text}
              format="LL"
              style={{
                width: 250,
                textAlign: 'right',
                color: 'grey',
                fontSize: 16,
              }}>
              {user.date_joined}
            </Moment>
          }
          bottomDivider
          chevron
        />
        <Separator />
        <Separator />
        <Separator />
        <Separator />
        <Separator />
        <Separator />
        <Separator />
        <Separator />
        <Separator />
        <Separator />
        <Separator />
        <Separator />
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.user.user,
  };
};

export default connect(mapStateToProps)(Profile);
