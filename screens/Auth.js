import React from 'react';
import {Container} from 'native-base';
import {StatusBar, View, Text, Image, ImageBackground} from 'react-native';
import {Button} from 'react-native-elements';
import Logo from '../assets/bg.png';
import Cap from '../assets/cap.png';
import Login from '../components/Login';
import Social from '../components/Social';
import {Card, Divider} from 'react-native-elements';
import styles from '../assets/styles';
import Regsiters from '../components/regsiters';
import AppIntroSlider from 'react-native-app-intro-slider';
import slides from '../assets/sliders';

export class Auth extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mode: true,
      showRealApp: true,
    };
    this.signUpMode = this.signUpMode.bind(this);
    this.loginMode = this.loginMode.bind(this);
  }

  _renderItem = item => {
    return (
      <View style={styles.mainView}>
        <Image style={styles.Image} source={item.item.image} />
        <Text style={styles.title}>{item.item.title}</Text>
        <Text style={styles.content}>{item.item.content}</Text>
        {item.index == 2 ? (
          <Button
            buttonStyle={{marginVertical: 50, borderRadius: 14}}
            containerStyle={{width: '60%'}}
            onPress={this._onDone}
            title="Get Started!!!"
          />
        ) : null}
      </View>
    );
  };

  _onDone = () => {
    this.setState({showRealApp: false});
  };

  signUpMode() {
    this.setState({mode: true});
  }

  loginMode() {
    this.setState({mode: false});
  }

  render() {
    if (this.state.showRealApp) {
      return (
        <AppIntroSlider
          activeDotStyle={{
            backgroundColor: '#358DCE',
            width: 40,
          }}
          dotStyle={{
            width: 40,
            backgroundColor: '#EFEFF4',
          }}
          renderItem={this._renderItem}
          slides={slides}
        />
      );
    } else {
      return (
        <Container>
          <StatusBar translucent barStyle="light-content" />
          <ImageBackground source={Logo} style={styles.imageBackround} />
          <Container style={styles.containerStyle}>
            <Card
              containerStyle={{
                width: 109,
                height: 109,
                marginVertical: 130,
                left: '33%',
                right: '33%',
                borderRadius: 16,
              }}>
              <Image
                source={Cap}
                style={{width: '70%', right: '15%', left: '15%'}}
              />
            </Card>
            <Card containerStyle={styles.cardStyle}>
              <View style={styles.switchView}>
                <View>
                  <Button
                    title="Sign Up"
                    type="clear"
                    titleStyle={
                      this.state.mode
                        ? styles.switchButtonActive
                        : styles.switchButton
                    }
                    onPress={this.signUpMode}
                  />
                  {this.state.mode ? (
                    <Divider style={styles.switchDivider} />
                  ) : (
                    <Divider style={{height: 0}} />
                  )}
                </View>
                <View>
                  <Button
                    title="Sign In"
                    type="clear"
                    titleStyle={
                      this.state.mode
                        ? styles.switchButton
                        : styles.switchButtonActive
                    }
                    onPress={this.loginMode}
                  />
                  {this.state.mode ? (
                    <Divider style={{height: 0}} />
                  ) : (
                    <Divider style={styles.switchDivider} />
                  )}
                </View>
              </View>
              <Divider style={{backgroundColor: '#D8D8D8'}} />
              {this.state.mode ? (
                <Login navigation={this.props.navigation} />
              ) : (
                <Regsiters navigation={this.props.navigation} />
              )}
            </Card>
            <Social />
          </Container>
        </Container>
      );
    }
  }
}

export default Auth;
