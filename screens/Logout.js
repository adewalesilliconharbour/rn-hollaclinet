import React, {Component} from 'react';
import {Text, View, ActivityIndicator} from 'react-native';

export class Logout extends Component {
  render() {
    return (
      <View>
        <View style={{flexDirection: 'column', justifyContent: 'center'}}>
          <ActivityIndicator
            style={{marginVertical: '100%'}}
            animating={true}
            size="large"
          />
        </View>
      </View>
    );
  }
}

export default Logout;
