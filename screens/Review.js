import React, {Component} from 'react';
import {View, StatusBar, Alert} from 'react-native';
import {
  Header,
  Left,
  Text,
  Icon,
  Button,
  Right,
  Body,
  Title,
  Content,
  Textarea,
} from 'native-base';
import {connect} from 'react-redux';
import {reviewWorker, completeJob} from '../store/actions/index';
import {Avatar, Rating, AirbnbRating} from 'react-native-elements';

export class Review extends Component {
  state = {
    id: this.props.worker,
    rating: 0.0,
    comment: '',
  };

  componentDidUpdate() {
    const {navigation, response} = this.props;
    if (response.isSuccess) {
      navigation.goBack();
    }
  }

  _completeJob = id => {
    const {review, completejob} = this.props;
    const reviewUser = {
      id,
      rating: this.state.rating,
      comment: this.state.comment,
    };
    review(reviewUser);
    completejob();
  };

  render() {
    const {navigation, job} = this.props;
    return (
      <View style={{backgroundColor: '#358DCE', height: '100%'}}>
        <StatusBar barStyle="light-content" />
        <Header transparent>
          <Left>
            <Button light transparent onPress={() => navigation.pop()}>
              <Icon name="arrow-back" />
            </Button>
            <Content>
              <Title>Rating</Title>
            </Content>
            <Right></Right>
          </Left>
        </Header>
        <View
          style={{
            justifyContent: 'center',
            flexDirection: 'column',
            flex: 1,
            marginHorizontal: 20,
          }}>
          <View
            style={{
              backgroundColor: '#fff',
              borderRadius: 10,
            }}>
            <View
              style={{
                alignItems: 'center',
                paddingHorizontal: 15,
                top: -50,
              }}>
              <Avatar rounded icon={{name: 'person'}} size={100} />
              <Text style={{marginTop: 10, fontSize: 17}}>
                {job
                  ? `${job.worker.firstName} ${job.worker.lastName}`
                  : 'loading....'}
              </Text>
              <Text style={{fontSize: 15, color: '#8A8A8F'}}>
                {job ? job.address : 'loading....'}
              </Text>
              <Text
                style={{
                  fontSize: 24,
                  fontWeight: '700',
                  marginTop: 30,
                  marginBottom: 15,
                }}>
                How was your experience?
              </Text>
              <Text style={{textAlign: 'center'}}>
                Your feedback will help improve driving experience
              </Text>
              <AirbnbRating
                showRating
                defaultRating={this.state.rating}
                onFinishRating={rating => this.setState({rating})}
              />
              <Textarea
                rowSpan={5}
                bordered
                onChangeText={comment => this.setState({comment})}
                placeholder="Additional comments..."
                style={{
                  width: '100%',
                  marginVertical: 30,
                  borderRadius: 12,
                  backgroundColor: '#EFEFF4',
                }}></Textarea>
              <Button
                disabled={this.state.rating == 0 ? true : false}
                block
                rounded
                onPress={() => this._completeJob(job.worker.id)}>
                <Text>Complete Job</Text>
              </Button>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    job: state.job.workerState,
    response: state.response,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    review: payload => dispatch(reviewWorker(payload)),
    completejob: payload => dispatch(completeJob(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Review);
