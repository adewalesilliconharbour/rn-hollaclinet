import React, {Component} from 'react';
import {Text, View, Alert, Modal} from 'react-native';
import {Header, Title, Left, Right} from 'native-base';
import {connect} from 'react-redux';
import DatePicker from 'react-native-datepicker';
import {Button} from 'react-native-elements';
import {updateJobs} from '../store/actions';
import moment from 'moment';

export class ConfirmPage extends Component {
  state = {
    schedule: false,
    date: '',
  };

  componentDidMount() {
    console.log(this.props);
  }

  _immmediate = status => {
    this.props.navigation.popToTop();
    const {editstatus} = this.props;
    const payload = {
      status: status,
    };
    editstatus(payload);
  };

  render() {
    return (
      <View>
        <Modal transparent visible={this.state.schedule}>
          <View
            style={{
              backgroundColor: 'rgba(0,0,0,0.5)',
              height: '100%',
              justifyContent: 'center',
              flexDirection: 'column',
              flex: 1,
            }}>
            <View
              style={{
                width: '90%',
                alignSelf: 'center',
                backgroundColor: '#fff',
                padding: 20,
                borderRadius: 12,
                shadowOpacity: 0.6,
                shadowRadius: 20,
              }}></View>
          </View>
        </Modal>
        <Header>
          <Left>
            <Button
              type="clear"
              icon={{name: 'arrow-back'}}
              onPress={() => this.props.navigation.pop()}
            />
          </Left>
          <Title>Job Visitation Time</Title>
          <Right></Right>
        </Header>
        <View
          style={{
            justifyContent: 'center',
            paddingHorizontal: 35,
            marginHorizontal: '3%',
            borderRadius: 12,
            backgroundColor: '#FFF',
            shadowOpacity: 0.4,
            padding: 30,
          }}>
          <Text style={{fontSize: 16, textAlign: 'center'}}>
            Please select one of the two options to confim visitation time
            agreed with the worker
          </Text>
          <Button
            containerStyle={{marginVertical: 20}}
            buttonStyle={{borderRadius: 12, backgroundColor: '#FFCE00'}}
            titleStyle={{}}
            title="Immediately"
            onPress={() => this._immmediate('enrouted')}
          />
          <Text style={{textAlign: 'center', marginBottom: 20}}>Or</Text>
          <DatePicker
            style={{width: '100%'}}
            date={this.state.date}
            mode="datetime"
            placeholder="Schedule for later date & time"
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            customStyles={{
              dateIcon: {
                position: 'absolute',
                left: 0,
                top: 4,
                marginLeft: 0,
              },
              dateInput: {
                marginLeft: 36,
                backgroundColor: '#03DE73',
                borderRadius: 12,
              },
              placeholderText: {
                fontWeight: '400',
                fontSize: 18,
                color: '#fff',
              },
            }}
            onDateChange={date => {
              this.setState({date});
              //   this.props.navigation.popToTop();
            }}
          />
          <Button
            containerStyle={{marginTop: 40, marginHorizontal: '30%'}}
            title="Proceed"
            onPress={() => {
              this.props.navigation.popToTop();
              Alert.alert(
                'Job Scheduled',
                'Cardi B - Plumber has been scheduled for 9am February 14th 2020. You will receive a notification an hour before the scheduled time',
              );
            }}
          />
          <Text style={{textAlign: 'center', marginTop: 10}}>
            Please press proceed after date & time have been selected
          </Text>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    job: state.job.workerState,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    // schedule: payload => dispatch(),
    editstatus: payload => dispatch(updateJobs(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ConfirmPage);
