import React, {Component} from 'react';
import {Text, View} from 'react-native';
import HeaderNav from '../components/Header';
import {connect} from 'react-redux';
import {Left, Body, Right, Icon, Separator, Container} from 'native-base';
import {Avatar, ListItem, Button} from 'react-native-elements';

export class Settings extends Component {
  render() {
    const {navigation, user} = this.props;
    return (
      <Container>
        <HeaderNav title="Settings" navigation={navigation} />
        <Separator />
        <ListItem
          leftAvatar={<Avatar rounded icon={{name: 'home'}} size="large" />}
          title={`${user.firstName} ${user.lastName}`}
          titleStyle={{fontSize: 20, marginBottom: 8}}
          subtitle={user.verification ? 'Verified Member' : 'Not Verified'}
          subtitleStyle={{color: '#8A8A8F', fontSize: 15}}
          chevron={{size: 22}}
          pad={22}
          onPress={() => navigation.navigate('profile')}
        />
        <Separator />
        <ListItem title="Notfications" chevron={{size: 22}} bottomDivider />
        <ListItem title="Security" chevron={{size: 22}} bottomDivider />
        <ListItem title="Language" chevron={{size: 22}} bottomDivider />
        <Separator />
        <ListItem title="Clear Cache" chevron={{size: 22}} bottomDivider />
        <ListItem
          title="Terms & Privacy Policy"
          chevron={{size: 22}}
          bottomDivider
          onPress={() => navigation.navigate('terms')}
        />
        <ListItem title="Contact Us" chevron={{size: 22}} bottomDivider />
        <Separator />
        <Separator />
        <Separator />
        <Separator />
        <Separator />
        <Button
          buttonStyle={{backgroundColor: '#FFF'}}
          titleStyle={{color: '#C8C7CC'}}
          title="Log Out"
        />
        <Separator />
        <Separator />
        <Separator />
        <Separator />
        <Separator />
        <Separator />
        <Separator />
        <Separator />
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.user.user,
  };
};

export default connect(mapStateToProps)(Settings);
