import React, {Component} from 'react';
import {Text, View} from 'react-native';
import HeaderNav from '../components/Header';
import {connect} from 'react-redux';
import {updateProfile} from '../store/actions/index';
import {Avatar, Input, Button} from 'react-native-elements';
import {Picker} from 'native-base';
import api from '../assets/connection';

import io from 'socket.io-client';

export class Editprofile extends Component {
  state = {
    avatar: this.props.users.avatar,
    firstName: this.props.users.firstName,
    lastName: this.props.users.lastName,
    email: this.props.users.email,
    phone: this.props.users.phone,
    sex: this.props.users.sex,
  };

  onSubmit() {
    const {updateprofile} = this.props;
    updateprofile(this.state);
  }

  componentDidMount() {
    this.socket = io(`${api.url}`);
  }

  componentDidUpdate() {
    const {navigation, response} = this.props;
    if (response.isSuccess) {
      this.socket.emit('refresh', {});
      navigation.goBack();
    }
  }

  render() {
    const {user} = this.props;
    const userSex = () => {
      if (this.state.sex == 'none') {
        return (
          <Picker
            placeholder="Select Your Gender"
            selectedValue={this.state.sex}
            iosHeader="Select your Gender"
            onValueChange={sex => this.setState({sex})}
            mode="dropdown">
            <Picker.Item label="Male" value="male" />
            <Picker.Item label="Female" value="female" />
          </Picker>
        );
      } else {
        return (
          <Input
            value={this.state.sex.replace(/^\w/, c => c.toUpperCase())}
            disabled
          />
        );
      }
    };

    return (
      <View>
        <HeaderNav title="Edit Profile" />
        <View
          style={{
            justifyContent: 'space-around',
            flexDirection: 'row',
            marginVertical: 30,
          }}>
          <View>
            <Avatar rounded icon={{name: 'person'}} editButton size="xlarge" />
            <Button
              style={{marginTop: 10}}
              type="clear"
              title="Edit Profile Photo"
            />
          </View>
        </View>
        <View style={{paddingHorizontal: 20}}>
          <Input
            placeholder="First Name"
            value={this.state.firstName}
            onChangeText={name => this.setState({firstName: name})}
          />
          <Input
            placeholder="Last Name"
            value={this.state.lastName}
            onChangeText={name => this.setState({lastName: name})}
          />
          <Input placeholder="Phone Number" value={this.state.phone} disabled />
          <Input placeholder="email" value={this.state.email} disabled />
          {userSex()}
          <Button
            buttonStyle={{marginTop: 30}}
            onPress={() => this.onSubmit()}
            title="SAVE"
          />
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  console.log(state);
  return {
    users: state.user.user,
    response: state.response,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateprofile: payload => dispatch(updateProfile(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Editprofile);
