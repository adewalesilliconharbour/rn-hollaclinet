import React, {Component, useEffect} from 'react';

import {Text, View} from 'react-native';
import {connect} from 'react-redux';
import {Header, Left, Button, Icon} from 'native-base';

export class Chat extends Component {
  componentDidMount() {
    console.log(this.props);
  }

  render() {
    const {navigation} = this.props;
    return (
      <View>
        <Header>
          <Left>
            <Button dark transparent onPress={() => navigation.pop()}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
        </Header>
        <Text> textInComponent </Text>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    clients: state.job.workerState,
    chats: state.job.workerState.conversation,
  };
};

export default connect(mapStateToProps)(Chat);
