import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import {
  View,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  Linking,
} from 'react-native';
import {
  Header,
  Left,
  Button,
  Item,
  Title,
  Right,
  DatePicker,
} from 'native-base';
import Moment from 'react-moment';
import AsyncStorage from '@react-native-community/async-storage';
import style from '../assets/styles';
import InputMessage from '../components/InputMessage';
import {Avatar, Icon} from 'react-native-elements';
import io from 'socket.io-client';
import api from '../assets/connection';

const _callWorker = worker => {
  let phoneNumber = '';
  if (Platform.OS === 'android') {
    phoneNumber = 'tel:${' + worker.worker.phone + '}';
  } else {
    phoneNumber = 'telprompt:${' + worker.worker.phone + '}';
  }
  Linking.openURL(phoneNumber);
};

const ChatPage = props => {
  var scrollView = null;
  const {worker} = props;
  const {conversation} = worker;
  const [typing, setTyping] = useState(false);
  const [id, setId] = useState('');

  const _getId = async () => {
    const id = await AsyncStorage.getItem('id');
    setId(id);
  };

  if (conversation.length == 0) {
    var message = <Text></Text>;
  } else {
    var message = conversation.map((m, i) => (
      <View
        key={i}
        style={id == m.senderId ? style.chatSender : style.chatReceiver}>
        <Text
          style={
            id == m.senderId ? style.chatsendertext : style.chatreceivertext
          }>
          {m.message}
        </Text>
        <Moment
          element={Text}
          format="LT"
          style={
            id == m.senderId ? style.chatSenderTimer : style.chatReceiverTimer
          }>
          {m.createdAt}
        </Moment>
      </View>
    ));
  }
  return (
    useEffect(() => {
      _getId();
      console.log(id);
      var socket = io(`${api.url}`);
      socket.on('is_typing', data => {
        if (data.receiver != worker.worker.id) {
          setTyping(true);
        }
      });
      socket.on('has_stopped_typing', data => {
        if (data.receiver != worker.worker.id) {
          setTyping(false);
        }
      });
    }),
    (
      <View
        style={{
          flexDirection: 'column',
          justifyContent: 'space-between',
          flex: 1,
        }}>
        <Header>
          <Left style={{flexDirection: 'row'}}>
            <Button dark transparent onPress={() => props.navigation.pop()}>
              <Icon name="arrow-back" />
              <Avatar
                containerStyle={{marginRight: 10}}
                rounded
                title={
                  worker
                    ? worker.worker.firstName.substring(0, 1)
                    : 'Loading...'
                }
              />
            </Button>
            <Button transparent>
              <Title>
                {worker
                  ? `${worker.worker.firstName} ${worker.worker.lastName}`
                  : 'Loading...'}
              </Title>
            </Button>
          </Left>
          <Right>
            <Button transparent onPress={() => _callWorker(worker)}>
              <Icon name="phone" />
            </Button>
          </Right>
        </Header>
        <ScrollView
          style={{marginBottom: 20}}
          ref={ref => {
            scrollView = ref;
          }}
          onContentSizeChange={(contentWidth, contentHeight) => {
            scrollView.scrollToEnd({animated: true});
          }}>
          <View>{message}</View>
          {typing ? (
            <View style={style.chatReceiver}>
              <Text style={style.chatreceivertext}>
                {`${worker.worker.firstName} ${worker.worker.lastName} is typing
                a message.....`}
              </Text>
            </View>
          ) : null}
        </ScrollView>
        <InputMessage navigation={props.navigation} />
      </View>
    )
  );
};

const mapStateToProps = state => {
  return {
    worker: state.job.workerState,
    chats: state.job.workerState.conversation,
  };
};

export default connect(mapStateToProps)(ChatPage);
