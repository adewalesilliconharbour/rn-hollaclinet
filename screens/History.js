import React, {Component} from 'react';
import {
  Text,
  View,
  StatusBar,
  Image,
  ScrollView,
  SafeAreaView,
} from 'react-native';
import HeaderNav from '../components/Header';
import {HistoryList} from '../assets/api';
import {Container, Card} from 'native-base';
import location from '../assets/location.png';
import money from '../assets/icons-money.png';
// import {ScrollView} from 'react-native-gesture-handler';
import {ListItem, Avatar} from 'react-native-elements';

export class History extends Component {
  render() {
    const {navigation} = this.props;
    var historyData = HistoryList.map((data, k) => (
      <Card
        key={k}
        style={{
          marginTop: 20,
          borderRadius: 12,
          shadowOpacity: 0.2,
          shadowRadius: 7,
          width: '90%',
          left: '5%',
        }}>
        <View>
          <View style={{flexDirection: 'row'}}>
            <View>
              <Image
                source={location}
                style={{
                  width: 24,
                  marginHorizontal: 5,
                  height: 100,
                  resizeMode: 'contain',
                  top: 10,
                  left: 7,
                }}
              />
            </View>
            <View style={{flex: 1}}>
              <ListItem
                containerStyle={{backgroundColor: 'transparent'}}
                title={`${data.worker.firstName} ${data.worker.lastName}`}
                subtitle={`${data.jobtitle}`}
                rightIcon={{name: 'star'}}
                rightElement={<Text>{`${data.worker.rating}.0`}</Text>}
              />
              <ListItem
                containerStyle={{backgroundColor: 'transparent'}}
                title={data.address}
              />
            </View>
          </View>
          <ListItem
            leftAvatar={
              <Avatar
                rounded
                size="small"
                overlayContainerStyle={{backgroundColor: '#66000000'}}
                source={money}
              />
            }
            containerStyle={{
              paddingVertical: 5,
              backgroundColor: 'transparent',
            }}
            titleStyle={{fontSize: 20}}
            title={`\u20A6 ${data.amount}`}
            rightElement={<Text style={{fontSize: 17}}>{data.status}</Text>}
            chevron
            topDivider
          />
        </View>
      </Card>
    ));
    return (
      <Container>
        <StatusBar barStyle="light-content" />
        <HeaderNav title="History" navigation={navigation} />
        <View
          style={{
            width: '100%',
            top: -90,
          }}>
          <SafeAreaView>
            <ScrollView>{historyData}</ScrollView>
          </SafeAreaView>
        </View>
      </Container>
    );
  }
}

export default History;
