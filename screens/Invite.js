import React, {Component} from 'react';
import {Text, View, ImageBackground, StatusBar, Image} from 'react-native';
import Invitebg from '../assets/invitebg.png';
import invite from '../assets/invite.png';
import {connect} from 'react-redux';
import HeaderNav from '../components/Header';
import {ListItem, Button} from 'react-native-elements';

export class Invite extends Component {
  render() {
    const {navigation, referralCode} = this.props;
    return (
      <View>
        <StatusBar barStyle="light-content" />
        <ImageBackground
          source={Invitebg}
          style={{width: '100%', height: 650, resizeMode: 'contain'}}>
          <HeaderNav title="Invite Friends" navigation={navigation} />
          <Image source={invite} style={{width: '100%', marginTop: 20}} />
          <Text
            style={{
              color: '#fff',
              fontSize: 33,
              fontWeight: '700',
              paddingHorizontal: 20,
              textAlign: 'center',
            }}>
            Invite Friends Get 3 Coupons each!
          </Text>
          <Text
            style={{
              paddingHorizontal: 30,
              textAlign: 'center',
              marginTop: 20,
              color: '#fff',
              fontSize: 17,
            }}>
            When your friend sign up wwith your referral code, you'll both get
            3.0 coupons
          </Text>
        </ImageBackground>
        <View style={{paddingHorizontal: 30, marginTop: 30}}>
          <Text style={{fontSize: 17}}>Share Your Invite Code</Text>
          <ListItem
            containerStyle={{
              borderBottomWidth: 2,
              borderBottomColor: '#000',
              marginVertical: 10,
            }}
            titleStyle={{fontSize: 24, fontWeight: '700'}}
            title={referralCode}
            rightIcon={{name: 'home'}}
          />
          <Button
            buttonStyle={{borderRadius: 8, marginTop: 12}}
            title="Invite Friends"
          />
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    referralCode: state.user.user.refCode,
  };
};

export default connect(mapStateToProps)(Invite);
