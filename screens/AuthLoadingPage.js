import React, {Component} from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import {ActivityIndicator, StatusBar, View} from 'react-native';

export class AuthLoadingScreen extends Component {
  componentDidMount() {
    this._getToken();
  }

  _getToken = async () => {
    const token = await AsyncStorage.getItem('auth_token');
    this.props.navigation.navigate(token ? 'App' : 'Auth');
  };

  render() {
    return (
      <View>
        <ActivityIndicator />
        {/* <StatusBar barStyle="default" /> */}
      </View>
    );
  }
}

export default AuthLoadingScreen;
