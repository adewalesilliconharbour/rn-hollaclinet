import React, {Component} from 'react';
import {Text, View, ImageBackground, StatusBar} from 'react-native';
import Bg from '../assets/bg.png';
import {connect} from 'react-redux';
import {initresponse, verifyUser} from '../store/actions/index';
import {Input, Button} from 'react-native-elements';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import {
  Container,
  Header,
  Body,
  Title,
  Subtitle,
  Left,
  Icon,
} from 'native-base';

export class Otp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      otp: '',
      // rights: 'client',
    };
  }

  componentDidMount() {
    const {initRep} = this.props;
    initRep();
  }

  componentDidUpdate() {
    const {response, navigation} = this.props;
    if (response.isSuccess) {
      navigation.navigate('App');
    }
  }

  pin(code) {
    this.setState({otp: code});
  }

  verify() {
    const {verifyotp} = this.props;
    verifyotp(this.state);
  }

  render() {
    return (
      <Container>
        <StatusBar translucent barStyle="light-content" />
        <View style={{height: 190}}>
          <ImageBackground
            source={Bg}
            style={{width: '100%', height: 390, resizeMode: 'contain'}}
          />
          <Body
            style={{
              top: 90,
              alignItems: 'flex-start',
              width: '100%',
              position: 'absolute',
            }}>
            <Title
              style={{
                fontSize: 34,
                fontWeight: '500',
                textAlign: 'right',
                color: '#fff',
              }}>
              Phone Verification
            </Title>
            <Subtitle style={{fontSize: 17, marginVertical: 12, color: '#fff'}}>
              Enter your OTP code here
            </Subtitle>
          </Body>
        </View>
        <View
          style={{
            backgroundColor: '#fff',
            height: '100%',
            paddingHorizontal: 70,
          }}>
          <OTPInputView
            pinCount={4}
            style={{height: '20%'}}
            onCodeChanged={code => {
              this.pin(code);
            }}
            secureTextEntry={true}
            codeInputFieldStyle={{
              width: 50,
              borderWidth: 0,
              borderBottomWidth: 5,
              fontSize: 36,
            }}
            onCodeFilled={code => {
              this.pin(code);
            }}
          />
          <Button
            title="Verify Now"
            onPress={() => this.verify()}
            disabled={this.state.otp.length != 4 ? true : false}
            buttonStyle={{borderRadius: 14, height: 50}}
          />
        </View>
      </Container>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    initRep: () => dispatch(initresponse()),
    verifyotp: payload => dispatch(verifyUser(payload)),
  };
};

const mapStateToProps = state => {
  return {
    response: state.response,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Otp);
