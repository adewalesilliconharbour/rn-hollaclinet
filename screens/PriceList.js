import React, {Component} from 'react';
import {View, Alert, Modal, Linking} from 'react-native';
import InAppBrowser from 'react-native-inappbrowser-reborn';
import {
  Header,
  Left,
  Button,
  Icon,
  Body,
  Title,
  Text,
  Right,
  ActionSheet,
  Toast,
} from 'native-base';
import {connect} from 'react-redux';
import api from '../assets/connection';
import {agreeprice, updateJobs} from '../store/actions/index';
import {ListItem} from 'react-native-elements';
import axios from 'axios';

var BUTTONS = ['Pay with Cash', 'E payment', 'Cancel'];
var CANCEL_INDEX = 4;

export class PriceList extends Component {
  state = {
    Epayment: false,
    isloading: false,
  };
  componentDidMount() {
    // console.log(this.props.response);
  }
  componentDidUpdate() {
    const {navigation, response} = this.props;
    if (response.isSuccess) {
      navigation.goBack();
    }
  }

  _processPayment = (id, email) => {
    this.setState({isloading: true});
    const payment = {
      app: `${api.appId}`,
      amount: `${this._sumTotal()}`,
      desc: 'Pay Holla Worker',
      job_id: id,
      email,
    };
    setTimeout(async () => {
      await axios.post(`${api.initPay}`, payment).then(
        async resp => {
          console.log(resp.data.url);
          const url = resp.data.url;
          const available = await InAppBrowser.isAvailable();
          if (available) {
            await InAppBrowser.open(url, {
              // iOS Properties
              dismissButtonStyle: 'cancel',
              preferredBarTintColor: 'gray',
              preferredControlTintColor: 'white',
              // Android Properties
              showTitle: true,
              toolbarColor: '#6200EE',
              secondaryToolbarColor: 'black',
              enableUrlBarHiding: true,
              enableDefaultShare: true,
              forceCloseOnRedirection: true,
            }).then(
              resp => {
                console.log(resp.type);
              },
              err => {
                console.log(err);
              },
            );
          } else {
            Linking.openURL(url);
          }
          this.setState({isloading: false});
        },
        err => {
          console.log(err.response);
          this.setState({isloading: false});
        },
      );
    }, 1000);
  };

  _sumTotal = () => {
    const {billing} = this.props;
    const total =
      parseInt(billing.payment.material) +
      parseInt(billing.payment.labour) +
      parseInt(billing.payment.transportation) +
      parseInt(billing.payment.others);
    return total;
  };

  _getTotal = () => {
    const {billing} = this.props;
    const total =
      parseInt(billing.payment.material) +
      parseInt(billing.payment.labour) +
      parseInt(billing.payment.transportation) +
      parseInt(billing.payment.others);

    return new Intl.NumberFormat('ja-JP', {
      style: 'currency',
      currency: 'NGN',
    }).format(total);
  };

  _format = price => {
    return new Intl.NumberFormat('ja-JP', {
      style: 'currency',
      currency: 'NGN',
    }).format(price);
  };

  _rejectPrice = status => {
    const {reject} = this.props;
    const payload = {
      status: status,
    };
    reject(payload);
  };

  _selectpayment = option => {
    const {agree} = this.props;
    switch (option) {
      case 'Pay with Cash':
        const payload = {
          method: 'cash',
        };
        agree(payload);
        break;
      case 'E payment':
        this.setState({Epayment: true});
        break;
      default:
        null;
    }
  };

  _actionsheet = () => {
    ActionSheet.show(
      {
        title: 'Select Payment Method',
        options: BUTTONS,
        cancelButtonIndex: CANCEL_INDEX,
      },
      buttonIndex => {
        this._selectpayment(BUTTONS[buttonIndex]);
      },
    );
  };

  render() {
    const {navigation, billing, user} = this.props;
    const jobId = billing.id;
    const email = user.email;
    return (
      <View>
        <View>
          <Modal transparent visible={this.state.Epayment}>
            <View
              style={{
                backgroundColor: 'rgba(0,0,0,0.5)',
                height: '100%',
                justifyContent: 'center',
                flexDirection: 'column',
                flex: 1,
              }}>
              <View
                style={{
                  // height: '40%',
                  width: '90%',
                  alignSelf: 'center',
                  backgroundColor: '#fff',
                  padding: 20,
                  borderRadius: 12,
                  shadowOpacity: 0.6,
                  shadowRadius: 20,
                }}>
                <Title style={{}}>E-PAYMENT</Title>
                <ListItem
                  title="Materials"
                  rightTitleStyle={{width: 250, textAlign: 'right'}}
                  rightTitle={this._format(billing.payment.material)}
                />
                <ListItem
                  title="Labour"
                  rightTitleStyle={{width: 250, textAlign: 'right'}}
                  rightTitle={this._format(billing.payment.labour)}
                />
                <ListItem
                  title="Transportation"
                  rightTitleStyle={{width: 250, textAlign: 'right'}}
                  rightTitle={this._format(billing.payment.transportation)}
                />
                <ListItem
                  title="Others"
                  rightTitleStyle={{width: 250, textAlign: 'right'}}
                  rightTitle={this._format(billing.payment.others)}
                  bottomDivider
                />
                <ListItem
                  title="Total"
                  rightTitleStyle={{width: 250, textAlign: 'right'}}
                  rightTitle={this._getTotal()}
                />
                {!this.state.isloading ? (
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      marginTop: 10,
                    }}>
                    <Button
                      block
                      style={{flex: 1, marginRight: 5}}
                      onPress={() => this._processPayment(jobId, email)}>
                      <Text>Proceed</Text>
                    </Button>
                    <Button
                      danger
                      block
                      style={{flex: 1, marginLeft: 5}}
                      onPress={() => this.setState({Epayment: false})}>
                      <Text>Cancel</Text>
                    </Button>
                  </View>
                ) : (
                  <Button block disabled style={{marginTop: 10}}>
                    <Text>Connecting to Payment Gateway .....</Text>
                  </Button>
                )}
                <Text style={{textAlign: 'center', marginTop: 50}}>
                  Powered By Ojapay and Unified Payment
                </Text>
              </View>
            </View>
          </Modal>
        </View>
        <Header>
          <Left>
            <Button dark transparent onPress={() => navigation.pop()}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>Price Billing</Title>
          </Body>
          <Right></Right>
        </Header>
        <ListItem
          title="Materials"
          rightTitle={this._format(billing.payment.material)}
          bottomDivider
        />
        <ListItem
          title="Labour"
          rightTitle={this._format(billing.payment.labour)}
          bottomDivider
        />
        <ListItem
          title="Transportation"
          rightTitle={this._format(billing.payment.transportation)}
          bottomDivider
        />
        <ListItem
          title="Others"
          rightTitle={this._format(billing.payment.others)}
          bottomDivider
        />
        <ListItem title="Total" rightTitle={this._getTotal()} />
        <Button
          style={{width: 250, alignSelf: 'center', marginTop: 30}}
          block
          onPress={() => this._actionsheet()}
          rounded>
          <Text>AGREE PRICE</Text>
        </Button>
        <Button
          danger
          style={{width: 250, alignSelf: 'center'}}
          block
          onPress={() => this._rejectPrice('atLocation')}
          transparent>
          <Text>REJECT PRICE</Text>
        </Button>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    billing: state.job.workerState,
    response: state.response,
    user: state.user.user,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    agree: payload => dispatch(agreeprice(payload)),
    reject: payload => dispatch(updateJobs(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PriceList);
