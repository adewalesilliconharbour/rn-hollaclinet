import React, {Component} from 'react';
import {Text, View} from 'react-native';
import {Header} from 'native-base';
import {WebView} from 'react-native-webview';

export class Epayment extends Component {
  componentDidMount() {
    console.log(this.props.navigation.state.params);
  }

  render() {
    return (
      <View>
        <Header />
        <WebView
          source={{uri: this.props.navigation.state.params}}
          //   style={{marginTop: 20}}
        />
      </View>
    );
  }
}

export default Epayment;
