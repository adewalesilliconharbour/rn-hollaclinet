import React, {Component} from 'react';
import {Text, View} from 'react-native';
import HeaderNav from '../components/Header';
import {Container} from 'native-base';

export class Wallet extends Component {
  render() {
    const {navigation} = this.props;
    return (
      <Container>
        <HeaderNav title="My Wallet" navigation={navigation} />
        <Text> textInComponent </Text>
      </Container>
    );
  }
}

export default Wallet;
