import slide1 from './slide1.png';
import slide2 from './slide2.png';
import slide3 from './slide3.png';

const slides = [
  {
    key: 'slide-1',
    image: slide1,
    title: 'Request Services',
    content: 'Holla at us for all kinds of services on demand',
  },
  {
    key: 'slide-2',
    image: slide2,
    title: 'Find A Service',
    content:
      'Choose the services that most fits your needs from any location within Lagos',
  },
  {
    key: 'slide-3',
    image: slide3,
    title: 'Confirm Worker',
    content:
      'Know the worker in advance and be able to view current location in real time on the map',
  },
];

export default slides;
