const api = {
  url: 'http://127.0.0.1:8000/',
  // url: 'https://hollatest.herokuapp.com/',
  version: 'api/v1/',
  auth: 'api/v1/auth/',
  login: 'signinotp/',
  register: 'registerotp/',
  verifyOtp: 'verifyotp/',
  key: 'AIzaSyBsfz_cNlv85hIspQlggF4hWjGGSYMp2BY',
  directionKey: 'AIzaSyAMvqQoFpllJEewjwEUwgY5nMaWF8wflGQ',
  appId: 'dwjoQBFBotG7aYG5RA0NguZBmPDhYb',
  initPay: 'https://test-upay.herokuapp.com/pay/init',
  transactionStatus: ' https://test-upay.herokuapp.com/pay/status',
};

export default api;
