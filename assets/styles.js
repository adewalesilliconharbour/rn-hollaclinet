import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  form: {
    paddingVertical: 30,
    paddingHorizontal: '5%',
    flexDirection: 'column',
    justifyContent: 'space-evenly',
  },
  formConstainerStyle: {
    borderColor: '#D8D8D8',
    borderWidth: 2,
    borderRadius: 15,
  },
  inputStyle: {
    padding: '2%',
    marginVertical: '2%',
  },
  inputContainerStyle: {
    borderBottomWidth: 0,
  },
  phoneInput: {
    padding: 18,
    borderWidth: 2,
    borderRadius: 14,
    borderColor: '#D8D8D8',
  },
  buttonStyle: {
    borderRadius: 14,
    padding: 14,
    backgroundColor: '#358DCE',
  },
  cardStyle: {
    borderRadius: 16,
    shadowRadius: 10,
    padding: 0,
  },
  containerStyle: {
    flex: 1,
    position: 'absolute',
    backgroundColor: 'transparent',
    right: '.5%',
    left: '.5%',
    flexDirection: 'column',
    justifyContent: 'space-around',
    padding: '1%',
  },
  switchView: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingVertical: '4%',
  },
  switchButtonActive: {
    fontSize: 26,
    fontWeight: '400',
    color: '#262628',
  },
  switchButton: {
    fontSize: 26,
    fontWeight: '500',
    color: '#D8D8D8',
  },
  switchDivider: {
    height: 6,
    borderRadius: 50,
    backgroundColor: '#358DCE',
    width: '40%',
    position: 'relative',
    right: '30%',
    left: '30%',
  },
  imageBackround: {
    resizeMode: 'contain',
    width: '100%',
    height: '70%',
  },
  iconCap: {
    width: 109,
    height: 109,
    marginVertical: '40%',
    left: '33%',
    right: '33%',
    borderRadius: 16,
  },
  errorInput: {
    color: 'red',
    paddingHorizontal: 12,
    paddingVertical: 4,
  },

  chatSender: {
    backgroundColor: '#358DCE',
    marginTop: 15,
    padding: 12,
    marginHorizontal: 12,
    borderTopRightRadius: 20,
    maxWidth: '80%',
    alignSelf: 'flex-end',
    borderTopLeftRadius: 15,
    borderBottomLeftRadius: 15,
  },
  chatReceiver: {
    backgroundColor: '#FEFEFE',
    marginTop: 15,
    padding: 12,
    marginHorizontal: 12,
    borderTopRightRadius: 15,
    maxWidth: '80%',
    alignSelf: 'flex-start',
    borderTopLeftRadius: 20,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 15,
  },
  chatSenderTimer: {
    color: '#fff',
    fontStyle: 'italic',
    fontSize: 10,
    textAlign: 'right',
  },
  chatReceiverTimer: {
    color: 'grey',
    fontStyle: 'italic',
    fontSize: 10,
    textAlign: 'left',
  },
  chatsendertext: {
    fontSize: 15,
    marginBottom: 3,
    color: '#fff',
  },
  chatreceivertext: {
    fontSize: 15,
    marginBottom: 3,
  },
  mainView: {
    backgroundColor: '#fff',
    height: '100%',
    alignItems: 'center',
    paddingTop: '10%',
  },
  title: {
    fontSize: 30,
    marginVertical: 60,
  },
  Image: {
    width: 320,
    height: 320,
    resizeMode: 'contain',
  },
  content: {
    paddingHorizontal: 65,
    textAlign: 'center',
    fontSize: 18,
  },
  buttonCircle: {
    width: 40,
    height: 40,
    backgroundColor: 'rgba(0, 0, 0, .2)',
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default styles;
